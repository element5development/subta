<?php 
/*----------------------------------------------------------------*\

	DEFAULT CATEGORY ARCHIVE TEMPLATE

\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>


<!-- PAGE TITLE AND BANNER -->
<?php $background = get_field('videos_background_image', 'options'); ?>
<header class="post-head" style="background-image: url(<?php echo $background['sizes']['xlarge']; ?>);">
	<div class="is-extra-wide">
		<h1><?php the_field('videos_title', 'option'); ?></h1>
		<?php if ( get_field('videos_description', 'option') ) : ?>
			<p><?php the_field('videos_description', 'option'); ?></p>
		<?php endif; ?>
		<button data-micromodal-trigger="modal-newsletter" class="is-green has-icon">
			<span>Subscribe</span>
			<svg>
				<use xlink:href="#icon-email" />
			</svg>
		</button>
	</div>
</header>

<div class="modal micromodal-slide" id="modal-newsletter" aria-hidden="true">
	<div class="modal__overlay" tabindex="-1" data-micromodal-close>
		<div class="modal__container" role="dialog" aria-modal="true" aria-labelledby="modal-1-title">
			<header class="modal__header">
				<h2>Direct to Your Inbox</h2>
				<button class="modal__close" aria-label="Close modal" data-micromodal-close></button>
			</header>
			<main class="modal__content newsletter" id="modal-1-content">
				<p>Be notified via email with the latest articles, industry news, partner resources and much more.</p>
				<?php echo do_shortcode('[gravityform id="3" title="false" description="false"]'); ?>
			</main>
		</div>
	</div>
</div>

<main id="main-content" class="full-width">
	<article>
		<?php 
			$terms = get_terms(
				array(
						'taxonomy'   => 'show',
						'hide_empty' => true,
						'exclude'    => 130,
				)
			);
		?>
		<?php if ( ! empty( $terms ) && is_array( $terms ) ) : ?>
			<section class="shows is-extra-wide">
				<?php foreach ( $terms as $term ) : ?>
					<a href="<?php echo esc_url( get_term_link( $term ) ) ?>">
						<article>
							<div class="show-poster">
								<?php if ( get_field('poster', $term) ) : ?>
									<?php $image = get_field('poster', $term); ?>
									<img class="lazyload" data-expand="250" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['large']; ?>" data-srcset="<?php echo $image['sizes']['small']; ?> 300w, <?php echo $image['sizes']['medium']; ?> 700w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1200w"  alt="<?php echo $image['alt']; ?>">
								<?php else : ?>
									<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/show-default-cover.jpg" />
								<?php endif; ?>
							</div>
							<h3><?php echo $term->name; ?></h3>
						</article>
					</a>
				<?php endforeach; ?>
			</section>
		<?php endif; ?>

		<?php 
			$the_query = new WP_Query( array(
				'post_type' => 'video',
				'posts_per_page' => 1
			));	
		?>
		<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
			<section class="featured-video video-post-preview is-standard">
				<a href="<?php echo get_permalink($essential_post->ID); ?>">
					<div class="image-wrap">
						<?php if ( get_field('video_thumbnail', $essential_post->ID) ) : ?>
							<?php $image = get_field('video_thumbnail', $essential_post->ID); ?>
							<img class="lazyload" data-expand="250" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['large']; ?>" data-srcset="<?php echo $image['sizes']['small']; ?> 300w, <?php echo $image['sizes']['medium']; ?> 700w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1200w"  alt="<?php echo $image['alt']; ?>">
						<?php else : ?>
							<img class="lazyload" data-expand="250" data-sizes="auto" src="/wp-content/themes/subta/dist/images/default-post-graphic.png"  alt="SUBTA -">
						<?php endif; ?>
						<?php if ( get_field('access_level', $essential_post->ID) != 'public' ) : ?>
							<div class="gated">
								<svg>
									<use xlink:href="#gated" />
								</svg>
								<span>Member Only</span>
							</div>
						<?php endif; ?>
					</div>
					<div class="contents">
						<p class="meta">
							<span>Latest Episode</span>
							<?php if ( get_field('run_time', $essential_post->ID) ) : ?>
								<span><?php the_field('run_time', $essential_post->ID); ?> min</span>
							<?php endif; ?>
							<?php echo get_the_date('F j, Y', $essential_post->ID); ?>
						</p>
						<h2><?php echo get_the_title($essential_post->ID); ?></h2>
						<p><?php the_field('post_header_description', $essential_post->ID); ?></p>
						<!-- <?php if ( get_field('guest', $essential_post->ID) ) : ?>
							<p class="meta">Featuring</p>
							<div class="guests is-extra-wide">
								<?php while( have_rows('guest', $essential_post->ID) ) : the_row(); ?>
									<div class="guest">
										<?php $image = get_sub_field('headshot'); ?>
										<img class="lazyload" data-expand="250" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['large']; ?>" data-srcset="<?php echo $image['sizes']['small']; ?> 300w, <?php echo $image['sizes']['medium']; ?> 700w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1200w"  alt="<?php echo $image['alt']; ?>" title="<?php the_sub_field('name'); ?>">
									</div>
								<?php endwhile; ?>
							</div>
						<?php endif; ?> -->
					</div>
				</a>
			</section>
		<?php endwhile; ?>

		<?php if ( get_field('videos_must_watch', 'options') ) : ?>
			<section class="topic-slider is-extra-wide">
				<h2>Must Watch</h2>
				<ul>
					<?php 
						$essential_posts = get_field('videos_must_watch', 'options');
						$i = 0;
						foreach( $essential_posts as $essential_post ):
							$i++;
					?>
						<li class="video-post-preview">
							<a href="<?php echo get_permalink($essential_post->ID); ?>">
								<?php if ( get_field('video_thumbnail', $essential_post->ID) ) : ?>
									<?php $image = get_field('video_thumbnail', $essential_post->ID); ?>
									<img class="lazyload" data-expand="250" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['large']; ?>" data-srcset="<?php echo $image['sizes']['small']; ?> 300w, <?php echo $image['sizes']['medium']; ?> 700w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1200w"  alt="<?php echo $image['alt']; ?>">
								<?php else : ?>
									<img class="lazyload" data-expand="250" data-sizes="auto" src="/wp-content/themes/subta/dist/images/default-post-graphic.png"  alt="SUBTA -">
								<?php endif; ?>
								<?php if ( get_field('access_level', $essential_post->ID) != 'public' ) : ?>
									<div class="gated">
										<svg>
											<use xlink:href="#gated" />
										</svg>
										<span>Member Only</span>
									</div>
								<?php endif; ?>
								<h3><?php echo get_the_title($essential_post->ID); ?></h3>
								<p class="meta">
									<?php if ( get_field('run_time', $essential_post->ID) ) : ?>
										<span><?php the_field('run_time', $essential_post->ID); ?> min</span>
									<?php endif; ?>
									<?php echo get_the_date('F j, Y', $essential_post->ID); ?>
								</p>
							</a>
						</li>
					<?php endforeach; ?>
				</ul>
			</section>
		<?php endif; ?>

		<?php 
			$terms = get_terms(
				array(
						'taxonomy'   => 'topic',
						'hide_empty' => true,
				)
			);
		?>
		<?php foreach ( $terms as $term ) : ?>
			<section class="topic-slider is-extra-wide">
				<h2>
					<?php echo $term->name; ?>
					<a href="<?php echo get_term_link($term); ?>">View All</a>
				</h2>
				<ul>
					<?php 
						$the_query = new WP_Query( array(
							'post_type' => 'video',
							'tax_query' => array(
									array (
											'taxonomy' => 'topic',
											'field' => 'slug',
											'terms' => $term->slug,
									)
							),
						));	
					?>
					<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
						<li class="video-post-preview">
							<a href="<?php echo get_permalink(); ?>">
								<?php if ( get_field('video_thumbnail') ) : ?>
									<?php $image = get_field('video_thumbnail'); ?>
									<img class="lazyload" data-expand="250" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['large']; ?>" data-srcset="<?php echo $image['sizes']['small']; ?> 300w, <?php echo $image['sizes']['medium']; ?> 700w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1200w"  alt="<?php echo $image['alt']; ?>">
								<?php else : ?>
									<img class="lazyload" data-expand="250" data-sizes="auto" src="/wp-content/themes/subta/dist/images/default-post-graphic.png"  alt="SUBTA -">
								<?php endif; ?>
								<?php if ( get_field('access_level') != 'public' ) : ?>
									<div class="gated">
										<svg>
											<use xlink:href="#gated" />
										</svg>
										<span>Member Only</span>
									</div>
								<?php endif; ?>
								<h3><?php echo get_the_title(); ?></h3>
								<p class="meta">
									<?php if ( get_field('run_time') ) : ?>
										<span><?php the_field('run_time'); ?> min</span>
									<?php endif; ?>
									<?php echo get_the_date('F j, Y'); ?>
								</p>
							</a>
						</li>
					<?php endwhile; wp_reset_postdata(); ?>
				</ul>
			</section>
		<?php endforeach; ?>
	</article>
</main>

<aside class="sub-or-join is-extra-wide">
	<div class="newsletter">
		<h2>Direct to Your Inbox</h2>
		<p>Be notified via email with the latest articles, industry news, partner resources and much more.</p>
		<?php echo do_shortcode('[gravityform id="3" title="false" description="false"]'); ?>
	</div>
	<div class="join">
		<h2>Unlock Everything</h2>
		<p>SUBTA is an ever-growing community of innovators, entrepreneurs, thought leaders and dedicated teams that are eager to scale their businesses and catalyze the subscription industry. Sound like you?</p>	
		<a href="/join" class="button is-yellow">Join SUBTA</a>
	</div>
</aside>

<?php get_template_part('template-parts/sections/post-footer'); ?>

<?php get_footer(); ?>