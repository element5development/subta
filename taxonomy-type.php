<?php 
/*----------------------------------------------------------------*\

	PARTNER'S 'TYPE' TAXONOMY

\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<main id="main-content" class="full-width">

  <!-- PAGE TITLE AND BANNER -->
  <header class="post-head">
		<div class="is-standard">
			<?php
				if ( function_exists('yoast_breadcrumb') ) :
					yoast_breadcrumb('<nav class="breadcrumbs">','</nav>');
				endif;
			?>
			<h1>Partners</h1>
		</div>
	</header>

	<!-- CATEOGRY FILTER -->
	<nav class="partner-categories">
		<?php //determine active category
			$term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) ); 
			if ( is_tax() ) : 
				$activeCat = $term->name;
			else :
				$activeCat = 'All';
			endif; 
		?>
		<?php	if ( $activeCat == 'All' ) : //check for active ?>
			<a href="/partners/" class="is-active">View All</a>
		<?php else : ?>
			<a href="/partners/">View All</a>
		<?php endif; ?>
		<?php //generate taxonomy list
			$terms = get_terms([
					'taxonomy' => 'type',
					'hide_empty' => true,
			]);
		?>
		<?php foreach ( $terms as $term ) :	?>
			<?php	if ( $activeCat == $term->name ) : //check for active ?>
				<a href="<?php echo get_term_link($term->slug, 'type'); ?>" class="is-active"><?php echo $term->name; ?></a>
			<?php else : ?>
				<a href="<?php echo get_term_link($term->slug, 'type'); ?>"><?php echo $term->name; ?></a>
			<?php endif; ?>
		<?php endforeach; ?>
	</nav>

	<!-- PARTNER GRID -->
	<?php if (have_posts()) : ?>
		<section class="partners">
			<?php	while ( have_posts() ) : the_post(); ?>
				<div class="partner">
					<?php $image = get_field('logo'); ?>
					<div class="logo">
						<img src="<?php echo esc_url($image['sizes']['small']); ?>" alt="<?php echo esc_attr($alt); ?>" />
					</div>
					<div class="level <?php the_field('sponsor_level'); ?>"><?php the_field('sponsor_level'); ?></div>
					<div class="contents">
						<p><?php the_field('company_description'); ?></p>
						<?php if ( get_field('member_only_deal') ) : ?>
							<div class="deal">
								<svg>
									<use xlink:href="#star" />
								</svg>
								Member Only Discount Available
								<svg>
									<use xlink:href="#star" />
								</svg>
							</div>
						<?php endif; ?>
					</div>
					<a href="<?php the_field('sponsor_link'); ?>" class="button is-arrow">
						Visit Website
					</a>
				</div>
			<?php endwhile; ?>
		</section>
	<?php endif; ?>
	<?php clean_pagination(); ?>
</main>

<?php get_template_part( 'template-parts/sections/footers/footer' ); ?>

<?php get_footer(); ?>