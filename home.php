<?php 
/*----------------------------------------------------------------*\

	DEFAULT POST ARCHIVE TEMPLATE

\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>


<?php $background = get_field('blog_background_image', 'options'); ?>
<header class="post-head" style="background-image: url(<?php echo $background['sizes']['xlarge']; ?>);">
	<div class="is-extra-wide">
		<h1><?php the_field('blog_title', 'option'); ?></h1>
		<?php if ( get_field('blog_description', 'option') ) : ?>
			<p><?php the_field('blog_description', 'option'); ?></p>
		<?php endif; ?>
		<button data-micromodal-trigger="modal-newsletter" class="is-green has-icon">
			<span>Subscribe</span>
			<svg>
				<use xlink:href="#icon-email" />
			</svg>
		</button>
	</div>
</header>


<div class="modal micromodal-slide" id="modal-newsletter" aria-hidden="true">
	<div class="modal__overlay" tabindex="-1" data-micromodal-close>
		<div class="modal__container" role="dialog" aria-modal="true" aria-labelledby="modal-1-title">
			<header class="modal__header">
				<h2>Direct to Your Inbox</h2>
				<button class="modal__close" aria-label="Close modal" data-micromodal-close></button>
			</header>
			<main class="modal__content newsletter" id="modal-1-content">
				<p>Be notified via email with the latest articles, industry news, partner resources and much more.</p>
				<?php echo do_shortcode('[gravityform id="3" title="false" description="false"]'); ?>
			</main>
		</div>
	</div>
</div>
       

<main id="main-content" class="full-width">

	<div class="featured-wrapper is-extra-wide">
		<?php if ( get_field('blog_featured_posts', 'options') ) : ?>
			<section class="featured-posts post-feed">
				<?php 
					$featured_posts = get_field('blog_featured_posts', 'options');
					foreach( $featured_posts as $featured_post ):
					if ( get_field('partner', $featured_post->ID) ) : 
						$partner_posts = get_field('partner', $featured_post->ID);
						foreach( $partner_posts as $partner_post ):
							$partner_name = get_the_title( $partner_post->ID );
						endforeach;
					endif;
				?>
					<article>
						<a href="<?php echo get_permalink( $featured_post->ID ); ?>">
							<div class="img-wrap">
								<?php if ( get_field('access_level', $featured_post->ID) != 'public' ) : ?>
									<div class="gated">
										<svg>
											<use xlink:href="#gated" />
										</svg>
										<span>Member Only</span>
									</div>
								<?php endif; ?>
								<?php if ( get_field('featured_image', $featured_post->ID) ) : ?>
									<?php $image = get_field('featured_image', $featured_post->ID); ?>
									<img class="lazyload" data-expand="250" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['large']; ?>" data-srcset="<?php echo $image['sizes']['small']; ?> 300w, <?php echo $image['sizes']['medium']; ?> 700w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1200w"  alt="<?php echo $image['alt']; ?>">
								<?php else : ?>
									<img class="lazyload" data-expand="250" data-sizes="auto" src="/wp-content/themes/subta/dist/images/default-post-graphic.png"  alt="SUBTA -">
								<?php endif; ?>
							</div>
							<p>
								<?php echo get_the_date('F d', $featured_post->ID); ?>
								<?php if ( get_field('estimated_read_time', $featured_post->ID) ) : ?>
									| <?php the_field('estimated_read_time', $featured_post->ID); ?> minute read
								<?php endif; ?>
							</p>
							<h3><?php echo get_the_title( $featured_post->ID ); ?></h3>
							<p>Written by <?php the_field('author', $featured_post->ID); ?><?php if (get_field('partner', $next_post->ID)) : ?> from <?php echo $partner_name; ?><?php endif; ?></p>
						</a>
					</article>
				<?php endforeach; ?>
			</section>
		<?php else : ?>
			<section class="featured-posts post-feed">
				<?php 
					$args = array(
						'posts_per_page' => 3, 
						'category__not_in' => 2, //industry news
					);
					$wp_query = new WP_Query( $args );
				?>
				<?php while($wp_query->have_posts()) : $wp_query->the_post(); ?>
					<article>
						<a href="<?php echo get_permalink( ); ?>">
							<div class="img-wrap">
								<?php if ( get_field('featured_image') ) : ?>
									<?php $image = get_field('featured_image'); ?>
									<img class="lazyload" data-expand="250" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['large']; ?>" data-srcset="<?php echo $image['sizes']['small']; ?> 300w, <?php echo $image['sizes']['medium']; ?> 700w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1200w"  alt="<?php echo $image['alt']; ?>">
								<?php else : ?>
									<img class="lazyload" data-expand="250" data-sizes="auto" src="/wp-content/themes/subta/dist/images/default-post-graphic.png"  alt="SUBTA -">
								<?php endif; ?>
							</div>
							<p>
								<?php echo get_the_date('F d',); ?>
								<?php if ( get_field('estimated_read_time') ) : ?>
									| <?php the_field('estimated_read_time'); ?> minute read
								<?php endif; ?>
							</p>
							<h3><?php echo get_the_title( ); ?></h3>
							<?php 
								if (get_field('partner')) :
									$partner_posts = get_field('partner');
									foreach( $partner_posts as $partner_post ):
										$partner_name = get_the_title( $partner_post->ID );
									endforeach;
								endif;
							?>
							<?php if ( get_field('author') || get_field('partner') ) : ?>
								<p>Written by <?php the_field('author'); ?><?php if (get_field('partner')) : ?> from <?php echo $partner_name; ?><?php endif; ?></p>
							<?php endif; ?>
						</a>
					</article>
				<?php endwhile; ?>
			</section>
		<?php endif; wp_reset_postdata(); ?>
		<?php 
			$args = array(
				'posts_per_page' => 6, 
				'cat' => 2, //industry news
			);
			$wp_query = new WP_Query( $args );
			if ($wp_query->have_posts()) :
		?>
			<section class="industry-news-feed post-feed">
				<h2>Industry News</h2>
				<?php while($wp_query->have_posts()) : $wp_query->the_post(); ?>
					<article>
						<a href="<?php echo get_permalink(); ?>">
							<p>
								<?php echo get_the_date('F d',); ?>
								<?php if ( get_field('estimated_read_time') ) : ?>
									| <?php the_field('estimated_read_time'); ?> minute read
								<?php endif; ?>
							</p>
							<h3><?php echo get_the_title(); ?></h3>
						</a>
					</article>
				<?php endwhile; wp_reset_postdata(); ?>
				<a href="<?php echo get_category_link('2'); ?>" class="button is-blue has-icon">
					<span>More Industry News</span>
					<svg>
						<use xlink:href="#next-arrow" />
					</svg>
				</a>
			</section>
		<?php wp_reset_query(); endif; ?>
	</div>

	<?php if ( get_field('blog_essential_reading', 'options') ) : ?>
		<section class="essential-posts post-feed is-extra-wide">
			<h2>Essential Reading</h2>
			<?php 
				$essential_posts = get_field('blog_essential_reading', 'options');
				$i = 0;;
				foreach( $essential_posts as $essential_post ):
			?>
				<?php $i++; ?>
				<article>
					<a href="<?php echo get_permalink( $essential_post->ID ); ?>">
						<p>
							<?php echo get_the_date('F d', $essential_post->ID); ?>
							<?php if ( get_field('estimated_read_time', $essential_post->ID) ) : ?>
								| <?php the_field('estimated_read_time', $essential_post->ID); ?> minute read
							<?php endif; ?>
						</p>
						<h3><?php echo get_the_title( $essential_post->ID ); ?></h3>
						<?php 
							if (get_field('partner', $essential_post->ID)) :
								$partner_posts = get_field('partner', $essential_post->ID);
								foreach( $partner_posts as $partner_post ):
									$partner_name = get_the_title( $partner_post->ID );
								endforeach;
							endif;
						?>
						<p>Written by <?php the_field('author', $essential_post->ID); ?><?php if (get_field('partner', $essential_post->ID)) : ?> from <?php echo $partner_name; ?><?php endif; ?></p>
						<span><?php echo $i; ?></span>
					</a>
				</article>
			<?php endforeach; ?>
		</section>
	<?php endif; ?>

	<aside class="subscribe-to-newsletter is-standard">
		<div class="newsletter">
			<h2>Direct to Your Inbox</h2>
			<p>Be notified via email with the latest articles, industry news, partner resources and much more.</p>
			<?php echo do_shortcode('[gravityform id="3" title="false" description="false"]'); ?>
		</div>
	</aside>

	<?php 
		$terms = get_terms(
			array(
					'taxonomy'   => 'postseries',
					'hide_empty' => false,
			)
		);
	?>
	<?php if ( ! empty( $terms ) && is_array( $terms ) ) : ?>
		<section class="reading-series is-extra-wide">
			<h2>Reading Series</h2>
			<div class="series">
				<?php foreach ( $terms as $term ) : ?>
					<a href="<?php echo esc_url( get_term_link( $term ) ) ?>">
						<article>
							<div class="series-cover">
								<?php if ( get_field('cover', $term) ) : ?>
									<?php $image = get_field('cover', $term); ?>
									<img class="lazyload" data-expand="250" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['large']; ?>" data-srcset="<?php echo $image['sizes']['small']; ?> 300w, <?php echo $image['sizes']['medium']; ?> 700w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1200w"  alt="<?php echo $image['alt']; ?>">
								<?php else : ?>
									<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/post-series-default-cover.jpg" />
								<?php endif; ?>
							</div>
							<h3><?php echo $term->name; ?></h3>
						</article>
					</a>
				<?php endforeach; ?>
				<div class="more-series-to-come">
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/post-series-cover-more.jpg" alt="more post series coming soon" />
				</div>
			</div>
		</section>
	<?php endif; ?>

	<nav class="cat-or-feed is-extra-wide">
		<button class="cat-toggle is-active">View Categories</button>
		<button class="latest-toggle">View Latest Posts</button>
	</nav>
	
	<?php if ( get_categories() ) : ?>
		<section class="categories is-active is-extra-wide">
			<ul>
				<?php $categories = get_categories(); foreach($categories as $category) : ?>
					<?php if ( $category->count > 0) : ?>
					<li>
						<a href="<?php echo get_category_link($category->term_id); ?>">
							<h3><?php echo $category->name; ?></h3>
							<p><?php echo $category->description; ?></p>
						</a>
					</li>
					<?php endif; ?>
				<?php endforeach; ?>
			</ul>
		</section>
	<?php endif; ?>

	<?php if (have_posts()) : ?>
		<section class="latest-posts is-extra-wide">
			<div class="infinite-feed post-feed">
				<?php	while ( have_posts() ) : the_post(); ?>
					<article>
						<a href="<?php echo get_permalink(); ?>">
							<div class="img-wrap">
								<?php if ( get_field('access_level') != 'public' ) : ?>
									<div class="gated">
										<svg>
											<use xlink:href="#gated" />
										</svg>
										<span>Member Only</span>
									</div>
								<?php endif; ?>
								<?php if ( get_field('featured_image') ) : ?>
									<?php $image = get_field('featured_image'); ?>
									<img class="lazyload" data-expand="250" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['large']; ?>" data-srcset="<?php echo $image['sizes']['small']; ?> 300w, <?php echo $image['sizes']['medium']; ?> 700w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1200w"  alt="<?php echo $image['alt']; ?>">
								<?php else : ?>
									<img class="lazyload" data-expand="250" data-sizes="auto" src="/wp-content/themes/subta/dist/images/default-post-graphic.png"  alt="SUBTA -">
								<?php endif; ?>
							</div>
							<p>
								<?php echo get_the_date('F d',); ?>
								<?php if ( get_field('estimated_read_time') ) : ?>
									| <?php the_field('estimated_read_time'); ?> minute read
								<?php endif; ?>
							</p>
							<h3><?php echo get_the_title(); ?></h3>
							<?php 
								if (get_field('partner')) :
									$partner_posts = get_field('partner');
									foreach( $partner_posts as $partner_post ):
										$partner_name = get_the_title( $partner_post->ID );
									endforeach;
								endif;
							?>
							<p>Written by <?php the_field('author'); ?><?php if (get_field('partner')) : ?> from <?php echo $partner_name; ?><?php endif; ?></p>
						</a>
					</article>
				<?php endwhile; ?>
			</div>
			<div class="page-load-status">
				<p class="infinite-scroll-request">
					<svg class="loading" x="0px" y="0px" width="40px" height="40px" viewBox="0 0 50 50" style="enable-background:new 0 0 50 50;">
						<path d="M43.935,25.145c0-10.318-8.364-18.683-18.683-18.683c-10.318,0-18.683,8.365-18.683,18.683h4.068c0-8.071,6.543-14.615,14.615-14.615c8.072,0,14.615,6.543,14.615,14.615H43.935z">
							<animateTransform attributeType="xml"
								attributeName="transform"
								type="rotate"
								from="0 25 25"
								to="360 25 25"
								dur="0.6s"
								repeatCount="indefinite"/>
						</path>
					</svg>
				</p>
				<p class="infinite-scroll-last"></p>
				<p class="infinite-scroll-error"></p>
			</div>
			<?php clean_pagination(); ?>
			<a class="load-more button is-blue">Load More</a>
		</section>
	<?php endif; ?>
</main>

<aside class="sub-or-join is-extra-wide">
	<div class="newsletter">
		<h2>Direct to Your Inbox</h2>
		<p>Be notified via email with the latest articles, industry news, partner resources and much more.</p>
		<?php echo do_shortcode('[gravityform id="3" title="false" description="false"]'); ?>
	</div>
	<div class="join">
		<h2>Unlock Everything</h2>
		<p>SUBTA is an ever-growing community of innovators, entrepreneurs, thought leaders and dedicated teams that are eager to scale their businesses and catalyze the subscription industry. Sound like you?</p>	
		<a href="/join" class="button is-yellow">Join SUBTA</a>
	</div>
</aside>

<?php get_template_part('template-parts/sections/post-footer'); ?>

<?php get_footer(); ?>