<?php 
/*----------------------------------------------------------------*\

	POST SERIES TAXONOMY FOR BLOG

\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php $term = get_queried_object(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<!-- PAGE TITLE AND BANNER -->
<header class="post-head is-standard">
	<?php if ( get_field('cover', $term) ) : ?>
		<?php $image = get_field('cover', $term); ?>
		<img class="lazyload" data-expand="250" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['large']; ?>" data-srcset="<?php echo $image['sizes']['small']; ?> 300w, <?php echo $image['sizes']['medium']; ?> 700w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1200w"  alt="<?php echo $image['alt']; ?>">
	<?php endif; ?>
	<div>
		<h1><span><?php echo $term->count; ?> post series on</span><?php single_cat_title() ?></h1>
		<?php echo category_description(); ?>
	</div>
</header>

<main id="main-content" class="full-width">
	<!-- PARTNER GRID -->
	<?php if (have_posts()) : ?>
		<section class="post-feed is-standard">
			<?php	while ( have_posts() ) : the_post(); ?>
				<article>
					<a href="<?php echo get_permalink(); ?>">
						<div class="img-wrap">
							<?php if ( get_field('featured_image') ) : ?>
								<?php $image = get_field('featured_image'); ?>
								<img class="lazyload" data-expand="250" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['large']; ?>" data-srcset="<?php echo $image['sizes']['small']; ?> 300w, <?php echo $image['sizes']['medium']; ?> 700w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1200w"  alt="<?php echo $image['alt']; ?>">
							<?php else : ?>
								<img class="lazyload" data-expand="250" data-sizes="auto" src="/wp-content/themes/subta/dist/images/default-post-graphic.png"  alt="SUBTA -">
							<?php endif; ?>
						</div>
						<p>
							<?php echo get_the_date('F d',); ?>
							<?php if ( get_field('estimated_read_time') ) : ?>
								| <?php the_field('estimated_read_time'); ?> minute read
							<?php endif; ?>
						</p>
						<h3><?php echo get_the_title(); ?></h3>
						<?php 
							if (get_field('partner')) :
								$partner_posts = get_field('partner');
								foreach( $partner_posts as $partner_post ):
									$partner_name = get_the_title( $partner_post->ID );
								endforeach;
							endif;
						?>
						<p>Written by <?php the_field('author'); ?><?php if (get_field('partner')) : ?> from <?php echo $partner_name; ?><?php endif; ?></p>
					</a>
				</article>
			<?php endwhile; ?>
		</section>
	<?php endif; ?>
	<?php clean_pagination(); ?>
</main>

<aside class="sub-or-join is-extra-wide">
	<div class="newsletter">
		<h2>Direct to Your Inbox</h2>
		<p>Be notified via email with the latest articles, industry news, partner resources and much more.</p>
		<?php echo do_shortcode('[gravityform id="3" title="false" description="false"]'); ?>
	</div>
	<div class="join">
		<h2>Unlock Everything</h2>
		<p>SUBTA is an ever-growing community of innovators, entrepreneurs, thought leaders and dedicated teams that are eager to scale their businesses and catalyze the subscription industry. Sound like you?</p>	
		<a href="/join" class="button is-yellow">Join SUBTA</a>
	</div>
</aside>

<?php get_template_part('template-parts/sections/post-footer'); ?>

<?php get_footer(); ?>