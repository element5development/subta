<?php 
/*----------------------------------------------------------------*\

	ARTICLE SECTION
	displaying selected posts

\*----------------------------------------------------------------*/
?>


<section id="section-<?php echo $template_args['sectionId']; ?>" class="featured-posts is-standard">
	<?php if ( get_sub_field('featured_posts_title') ) : ?>
		<div class="title">
			<h2><?php the_sub_field('featured_posts_title'); ?></h2>
			<?php if ( get_sub_field('view_all_button') ) : ?>
				<a href="/blog" class="button is-arrow">View All Posts</a>
			<?php endif; ?>
		</div>
	<?php endif; ?>
	<?php if ( get_sub_field('featured_posts_description') ) : ?>
		<p><?php the_sub_field('featured_posts_description'); ?></p>
	<?php endif; ?>
	<div>
		<?php $featured_posts = get_sub_field('posts'); ?>
		<?php foreach( $featured_posts as $post ): ?>
			<a class="post-preview" href="<?php echo get_permalink($post->ID); ?>">
				<article class="post-preview">
					<?php 
						$thumb_id = get_post_thumbnail_id($post->ID);
						$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'thumbnail', true);
						$thumb_url = $thumb_url_array[0];
					?>
					<?php if ( get_post_thumbnail_id($post->ID) ) : ?>
						<img src="<?php echo $thumb_url; ?>"  />
					<?php else : ?>
						<div class="image"></div>
					<?php endif; ?>
					<div>
						<h3><?php echo get_the_title($post->ID); ?></h3>
						<p><?php echo get_the_excerpt($post->ID); ?></p>
						<div class="button is-arrow">Read the Rest</div>
					</div>
				</article>
			</a>
		<?php endforeach; wp_reset_postdata(); ?>
	</div>
</section>
