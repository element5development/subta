<?php 
/*----------------------------------------------------------------*\

	ARTICLE SECTION
	displaying 2 columns one with a image the other with an editor

\*----------------------------------------------------------------*/
?>

<section id="section-<?php echo $template_args['sectionId']; ?>" class="price-text <?php the_sub_field('width'); ?> <?php the_sub_field('card_alignment'); ?>">
	<div class="price-cards">
		<?php while( have_rows('price_card') ) : the_row(); ?>
			<div>
				<h2><?php the_sub_field('title'); ?></h2>
				<?php if( get_sub_field('description') ) : ?>
					<p><?php the_sub_field('description'); ?></p>
				<?php endif; ?>
				<?php 
					if ( get_sub_field('type_two_label') || get_sub_field('type_two_price') ) :
						$class = "has-two-type";
					else :
						$class = "has-one-type";
					endif; 
				?>
				<div class="type-grid <?php echo $class; ?>">
					<div class="label spacer"></div>
					<div class="label type-one">
						<h3><?php the_sub_field('type_one_label'); ?></h3>
						<?php if( get_sub_field('type_one_price') ) : ?>
							<p><?php the_sub_field('type_one_price'); ?></p>
						<?php endif; ?>
					</div>
					<?php if ( $class == 'has-two-type' ) : ?>
						<div class="label type-two">
							<h3><?php the_sub_field('type_two_label'); ?></h3>
							<?php if( get_sub_field('type_two_price') ) : ?>
								<p><?php the_sub_field('type_two_price'); ?></p>
							<?php endif; ?>
						</div>
					<?php endif; ?>
					<?php while( have_rows('options') ) : the_row(); ?>
						<div class="option label"><?php the_sub_field('label'); ?></div>
						<div class="option type-one">
							<?php if ( get_sub_field('include_in_type_one') ) : ?>
								<svg viewBox="0 0 26 28">
									<use xlink:href="#checkmark-filled"></use>
								</svg>
							<?php else: ?>
								<svg viewBox="0 0 26 28">
									<use xlink:href="#checkmark-empty"></use>
								</svg>
							<?php endif; ?>
						</div>
						<?php if ( $class == 'has-two-type' ) : ?>
							<div class="option type-two">
								<?php if ( get_sub_field('include_in_type_two') ) : ?>
									<svg viewBox="0 0 26 28">
										<use xlink:href="#checkmark-filled"></use>
									</svg>
								<?php else: ?>
									<svg viewBox="0 0 26 28">
										<use xlink:href="#checkmark-empty"></use>
									</svg>
								<?php endif; ?>
							</div>
						<?php endif; ?>
					<?php endwhile; ?>
				</div>
				<?php if( get_sub_field('cta') ) : ?>
					<?php 
						$link = get_sub_field('cta');
						$link_url = $link['url'];
						$link_title = $link['title'];
						$link_target = $link['target'] ? $link['target'] : '_self';
					?>
					<a class="button is-yellow" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a>
				<?php endif; ?>
			</div>
		<?php endwhile; ?>
	</div>
	<div>
		<?php the_sub_field('content'); ?>
	</div>
</section>