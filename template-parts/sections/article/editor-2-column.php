<?php 
/*----------------------------------------------------------------*\

	ARTICLE SECTION
	containing a 2 wysiwyg editor

\*----------------------------------------------------------------*/
?>

<section id="section-<?php echo $template_args['sectionId']; ?>" class="editor-2-column <?php the_sub_field('width'); ?>">
	<div>
		<?php the_sub_field('content-left'); ?>
	</div>
	<div>
		<?php the_sub_field('content-right'); ?>
	</div>
</section>