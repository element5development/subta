<?php 
/*----------------------------------------------------------------*\

	ARTICLE SECTION
	displaying 2 columns one with a series of people and the other with an editor

\*----------------------------------------------------------------*/
?>

<section id="section-<?php echo $template_args['sectionId']; ?>" class="profile-text <?php the_sub_field('width'); ?> <?php the_sub_field('profile_alignment'); ?>">
	<div>
		<?php the_sub_field('content'); ?>
	</div>
	<div class="profiles">
		<?php if ( have_rows('profiles') ) : ?>
			<?php while( have_rows('profiles') ) : the_row(); ?>
				<div class="profile">
					<div class="headshot">
						<?php if ( get_sub_field('linkedin') ) : ?>
							<a target="_blank" href="<?php the_sub_field('linkedin'); ?>">
						<?php endif; ?>
								<?php $image = get_sub_field('headshot'); ?>
								<img class="lazyload blur-up" data-expand="250" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['large']; ?>" data-srcset="<?php echo $image['sizes']['small']; ?> 300w, <?php echo $image['sizes']['medium']; ?> 700w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1200w"  alt="<?php echo $image['alt']; ?>">
						<?php if ( get_sub_field('linkedin') ) : ?>	
								<svg>
									<use xlink:href="#linkedin" />
								</svg>
							</a>
						<?php endif; ?>
					</div>
					<div>
						<h3><?php the_sub_field('name'); ?></h3>
						<?php if ( get_sub_field('job_title') || get_sub_field('company_logo') ) : ?>
							<p>
								<?php if ( get_sub_field('job_title') ) : ?>
									<?php the_sub_field('job_title'); ?><?php if ( get_sub_field('company') ) : ?>, <?php the_sub_field('company'); ?><?php endif; ?>
								<?php endif; ?>
								<?php if ( get_sub_field('company_logo') ) : ?>
									<?php $image = get_sub_field('company_logo'); ?>
									<img class="lazyload blur-up" data-expand="250" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['large']; ?>" data-srcset="<?php echo $image['sizes']['small']; ?> 300w, <?php echo $image['sizes']['medium']; ?> 700w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1200w"  alt="<?php echo $image['alt']; ?>">
								<?php endif; ?>
							</p>
						<?php endif; ?>
					</div>
				</div>
			<?php endwhile; ?>
		<?php endif; ?>
	</div>
</section>