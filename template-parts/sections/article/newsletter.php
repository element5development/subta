<?php 
/*----------------------------------------------------------------*\

	ARTICLE SECTION
	displaying newsletter signup

\*----------------------------------------------------------------*/
?>
<section class="newsletter is-full-width">
	<?php if ( get_sub_field('title') ) : ?>
		<h2><?php the_sub_field('title'); ?></h2>
	<?php endif; ?>
	<?php if ( get_sub_field('description') ) : ?>
		<p><?php the_sub_field('description'); ?></p>
	<?php endif; ?>
	<?php echo do_shortcode('[gravityform id="'. get_sub_field('form_id') .'" title="false" description="false"]'); ?>
</section>