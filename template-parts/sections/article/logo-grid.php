<?php 
/*----------------------------------------------------------------*\

	ARTICLE SECTION
	displaying grid of logos

\*----------------------------------------------------------------*/
?>

<?php
	$logos = get_sub_field('logos');
	$columns = get_sub_field('columns');
?>

<section id="section-<?php echo $template_args['sectionId']; ?>" class="logo-grid <?php the_sub_field('width'); ?> columns-<?php echo $columns; ?>">
	<?php if ( get_sub_field('logo_grid_title') ) : ?>
		<h3><?php the_sub_field('logo_grid_title'); ?></h3>
	<?php endif; ?>
	<?php $logos = get_sub_field('logos'); ?>	
	<?php foreach( $logos as $logo ): ?>
		<img class="lazyload blur-up" data-expand="250" data-sizes="auto" src="<?php echo $logo['sizes']['placeholder']; ?>" data-src="<?php echo $logo['sizes']['large']; ?>" data-srcset="<?php echo $logo['sizes']['small']; ?> 350w, <?php echo $logo['sizes']['medium']; ?> 700w, <?php echo $logo['sizes']['large']; ?> 1000w, <?php echo $logo['sizes']['xlarge']; ?> 1200w"  alt="<?php echo $logo['alt']; ?>">
	<?php endforeach; ?>
</section>
