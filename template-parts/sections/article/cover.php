<?php 
/*----------------------------------------------------------------*\

	ARTICLE SECTION
	displaying content with background image

\*----------------------------------------------------------------*/
?>
<?php $image = get_sub_field('background'); ?>
<section id="section-<?php echo $template_args['sectionId']; ?>" class="cover <?php the_sub_field('width'); ?> <?php if (get_sub_field('overlay')) : ?>has-overlay<?php endif; ?> lazyload blur-up" style="color: <?php the_sub_field('text_color'); ?>" data-expand="250" data-bgset="<?php echo $image['sizes']['small']; ?> 350w, <?php echo $image['sizes']['medium']; ?> 700w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1200w" data-sizes="auto">
	<div>
		<?php the_sub_field('content'); ?>
	</div>
</section>
