<?php 
/*----------------------------------------------------------------*\

	ARTICLE SECTION
	displaying video embed within a responsive container

\*----------------------------------------------------------------*/
?>
<section id="section-<?php echo $template_args['sectionId']; ?>" class="responsive-video <?php the_sub_field('width'); ?>">
	<div>
		<?php the_sub_field('content'); ?>
	</div>
</section>