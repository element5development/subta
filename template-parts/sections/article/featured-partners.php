<?php 
/*----------------------------------------------------------------*\

	ARTICLE SECTION
	displaying selected partners

\*----------------------------------------------------------------*/
?>

<?php
	$columns = get_sub_field('columns');
?>

<section id="section-<?php echo $template_args['sectionId']; ?>" class="featured-partners is-standard columns-<?php echo $columns; ?>">
	<?php if ( get_sub_field('featured_partners_title') || get_sub_field('featured_partners_description') ) : ?>
		<div>
			<?php if ( get_sub_field('featured_partners_title') ) : ?>
				<div class="title">
					<h2><?php the_sub_field('featured_partners_title'); ?></h2>
					<?php if ( get_sub_field('view_all_button') ) : ?>
						<a href="/partners" class="button is-arrow">View All Partners</a>
					<?php endif; ?>
				</div>	
			<?php endif; ?>
			<?php if ( get_sub_field('featured_partners_description') ) : ?>
				<p><?php the_sub_field('featured_partners_description'); ?></p>
			<?php endif; ?>
		</div>
	<?php endif; ?>
	<?php $featured_partners = get_sub_field('partners'); ?>	
	<?php foreach( $featured_partners as $partner ): ?>
		<?php $image = get_field('logo', $partner->ID); ?>
		<img class="lazyload blur-up" data-expand="250" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['large']; ?>" data-srcset="<?php echo $image['sizes']['small']; ?> 300w, <?php echo $image['sizes']['medium']; ?> 700w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1200w"  alt="<?php echo $image['alt']; ?>">
	<?php endforeach; wp_reset_postdata(); ?>
</section>
