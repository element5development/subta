<?php 
/*----------------------------------------------------------------*\

	ARTICLE SECTION
	displaying slider of testimonials

\*----------------------------------------------------------------*/
?>

<section class="testimonials">
	<?php if ( get_sub_field('testimonial_title') ) : ?>
		<h2><?php the_sub_field('testimonial_title'); ?></h2>
	<?php endif; ?>
	<?php if ( get_sub_field('testimonial_description') ) : ?>
		<p><?php the_sub_field('testimonial_description'); ?></p>
	<?php endif; ?>
	<div class="testimonial-slider is-extra-wide">
		<?php while( have_rows('testimonials') ) : the_row(); ?>
			<div class="testimonial">
				<p><?php the_sub_field('quote') ?></p>
				<div class="quotee">
					<?php if ( get_sub_field('headshot') ) : ?>
						<div class="headshot">
							<?php $image = get_sub_field('headshot'); ?>
							<img class="lazyload" data-expand="250" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['large']; ?>" data-srcset="<?php echo $image['sizes']['small']; ?> 300w, <?php echo $image['sizes']['medium']; ?> 700w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1200w"  alt="<?php echo $image['alt']; ?>">
						</div>
					<?php endif; ?>
					<div>
						<h3><?php the_sub_field('name'); ?></h3>
						<p>
							<?php the_sub_field('job_title'); ?>
							<?php if ( get_sub_field('company_logo') ) : ?>
								<?php $image = get_sub_field('company_logo'); ?>
								<img class="lazyload" data-expand="250" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['large']; ?>" data-srcset="<?php echo $image['sizes']['small']; ?> 300w, <?php echo $image['sizes']['medium']; ?> 700w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1200w"  alt="<?php echo $image['alt']; ?>">
							<?php endif; ?>
						</p>
					</div>
				</div>
			</div>
		<?php endwhile; ?>
	</div>
</section>