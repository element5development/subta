<?php 
/*----------------------------------------------------------------*\

	ARTICLE SECTION
	displaying selected content post

\*----------------------------------------------------------------*/
?>
<section id="section-<?php echo $template_args['sectionId']; ?>" class="featured-content is-extra-wide">
	<div class="quote">
		<?php if ( get_sub_field('background_image') ) : ?>
			<?php $image = get_sub_field('background_image'); ?>
			<img class="background_image lazyload blur-up" data-expand="250" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['large']; ?>" data-srcset="<?php echo $image['sizes']['small']; ?> 350w, <?php echo $image['sizes']['medium']; ?> 700w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1200w"  alt="<?php echo $image['alt']; ?>">
		<?php endif; ?>
		<div data-emergence="hidden">
			<?php if ( get_sub_field('quote') ) : ?>
				<p><?php the_sub_field('quote'); ?></p>
			<?php endif; ?>
			<?php if ( get_sub_field('author_title') ) : ?>
				<div class="author">
					<?php if ( get_sub_field('author_headshot') ) : ?>
						<?php $headshot = get_sub_field('author_headshot'); ?>
						<img class="lazyload blur-up" data-expand="250" data-sizes="auto" src="<?php echo $headshot['sizes']['placeholder']; ?>" data-src="<?php echo $headshot['sizes']['large']; ?>" data-srcset="<?php echo $headshot['sizes']['small']; ?> 350w, <?php echo $headshot['sizes']['medium']; ?> 700w, <?php echo $headshot['sizes']['large']; ?> 1000w, <?php echo $headshot['sizes']['xlarge']; ?> 1200w"  alt="<?php echo $headshot['alt']; ?>">
					<?php endif; ?>
					<p><?php the_sub_field('author_title'); ?></p>
				</div>
			<?php endif; ?>
		</div>
	</div>
	<div class="content">
		<?php if ( get_sub_field('title') ) : ?>
			<h2>
				<?php if ( get_sub_field('label') ) : ?>
					<span><?php the_sub_field('label'); ?></span>
				<?php endif; ?>
				<?php the_sub_field('title'); ?>
			</h2>
		<?php endif; ?>
		<?php if ( get_sub_field('description') ) : ?>
			<?php the_sub_field('description'); ?>
		<?php endif; ?>
		<?php if ( get_sub_field('button') ) : ?>
			<?php 
				$link = get_sub_field('button'); 
				$link_url = $link['url'];
				$link_title = $link['title'];
				$link_target = $link['target'] ? $link['target'] : '_self'; 
			?>
			<a class="button <?php the_sub_field('color'); ?> <?php if ( get_sub_field('icon') ) : ?>has-icon<?php endif; ?>" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>">
				<?php if ( get_sub_field('icon') ) : ?>
					<?php $icon = get_sub_field('icon'); ?>
					<img class="lazyload blur-up" data-expand="250" data-sizes="auto" src="<?php echo $icon['sizes']['placeholder']; ?>" data-src="<?php echo $icon['sizes']['large']; ?>" data-srcset="<?php echo $icon['sizes']['small']; ?> 350w, <?php echo $icon['sizes']['medium']; ?> 700w, <?php echo $icon['sizes']['large']; ?> 1000w, <?php echo $icon['sizes']['xlarge']; ?> 1200w"  alt="<?php echo $icon['alt']; ?>">
				<?php endif; ?>
				<span><?php echo esc_html($link_title); ?></span>
			</a>
		<?php endif; ?>
	</div>
</section>