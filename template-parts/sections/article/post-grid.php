<?php 
/*----------------------------------------------------------------*\

	ARTICLE SECTION
	displaying post grid system

\*----------------------------------------------------------------*/
?>

<section id="section-<?php echo $template_args['sectionId']; ?>" class="post-grid is-extra-wide">
	<?php if ( get_sub_field('post_grid_title') ) : ?>
		<h2><?php the_sub_field('post_grid_title'); ?></h2>
	<?php endif; ?>
	<?php if ( get_sub_field('post_grid_description') ) : ?>
		<p><?php the_sub_field('post_grid_description'); ?></p>
	<?php endif; ?>
	<?php if ( have_rows('rows') ) : ?>
		<div class="grid-system">
			<?php while( have_rows('rows') ) : the_row(); ?>
				<div class="grid-system-row <?php the_sub_field('layout'); ?>" >
					<?php while( have_rows('cards') ) : the_row(); ?>
						<?php if ( get_sub_field('link_list') ) : ?>
							<div class="link-list">
								<?php if ( get_sub_field('title') ) : ?>
									<h2>
										<?php the_sub_field('title'); ?>
									</h2>
								<?php endif; ?>
								<?php $featured_posts = get_sub_field('link_list'); ?>
								<ul>
									<?php foreach( $featured_posts as $featured_post ): ?>
										<?php 
											$permalink = get_permalink( $featured_post->ID );
											$title = get_the_title( $featured_post->ID );
										?>
										<li>
											<a href="<?php echo esc_url( $permalink ); ?>"><?php echo esc_html( $title ); ?></a>
										</li>
									<?php endforeach; ?>
									<?php if ( get_sub_field('button') ) : ?>
										<?php 
											$link = get_sub_field('button'); 
											$link_url = $link['url'];
											$link_title = $link['title'];
											$link_target = $link['target'] ? $link['target'] : '_self'; 
										?>
										<li>
										<a class="button is-arrow" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>">
											<span><?php echo esc_html($link_title); ?></span>
										</a>
										</li>
									<?php endif; ?>
								</ul>
							</div>
						<?php else : ?>
							<?php $background = get_sub_field('background_image'); ?>
							<article class="<?php if ( get_sub_field('overlay') ) : ?>has-overlay<?php endif; ?> <?php if ( !get_sub_field('background_image') ) : ?>has-no-background<?php endif; ?>" <?php if ( get_sub_field('background_image') ) : ?>style="background-image: url(<?php echo $background['sizes']['xlarge']; ?>);"<?php endif; ?>>
								<?php if ( get_sub_field('logo') ) : ?>
									<?php $logo = get_sub_field('logo'); ?>
									<img class="logo lazyload blur-up" data-expand="250" data-sizes="auto" src="<?php echo $logo['sizes']['placeholder']; ?>" data-src="<?php echo $logo['sizes']['large']; ?>" data-srcset="<?php echo $logo['sizes']['small']; ?> 350w, <?php echo $logo['sizes']['medium']; ?> 700w, <?php echo $logo['sizes']['large']; ?> 1000w, <?php echo $logo['sizes']['xlarge']; ?> 1200w"  alt="<?php echo $logo['alt']; ?>">
								<?php endif; ?>
								<?php if ( get_sub_field('title') ) : ?>
									<h2>
										<?php if ( get_sub_field('label') ) : ?>
											<span><?php the_sub_field('label'); ?></span>
										<?php endif; ?>
										<?php the_sub_field('title'); ?>
									</h2>
								<?php endif; ?>
								<?php if ( get_sub_field('description') ) : ?>
									<p><?php the_sub_field('description'); ?></p>
								<?php endif; ?>
								<?php if ( have_rows('details') ) : ?>
									<ul>
										<?php while( have_rows('details') ) : the_row(); ?>
											<li>
												<?php if ( get_sub_field('icon') ) : ?>
													<?php $icon = get_sub_field('icon'); ?>
													<img class="lazyload blur-up" data-expand="250" data-sizes="auto" src="<?php echo $icon['sizes']['placeholder']; ?>" data-src="<?php echo $icon['sizes']['large']; ?>" data-srcset="<?php echo $icon['sizes']['small']; ?> 350w, <?php echo $icon['sizes']['medium']; ?> 700w, <?php echo $icon['sizes']['large']; ?> 1000w, <?php echo $icon['sizes']['xlarge']; ?> 1200w"  alt="<?php echo $icon['alt']; ?>">
												<?php endif; ?>
												<span><?php the_sub_field('label'); ?></span>
											</li>
										<?php endwhile; ?>
									</ul>
								<?php endif; ?>
								<?php if ( get_sub_field('author_title') ) : ?>
									<div class="author">
										<?php if ( get_sub_field('author_headshot') ) : ?>
											<?php $headshot = get_sub_field('author_headshot'); ?>
											<img class="lazyload blur-up" data-expand="250" data-sizes="auto" src="<?php echo $headshot['sizes']['placeholder']; ?>" data-src="<?php echo $headshot['sizes']['large']; ?>" data-srcset="<?php echo $headshot['sizes']['small']; ?> 350w, <?php echo $headshot['sizes']['medium']; ?> 700w, <?php echo $headshot['sizes']['large']; ?> 1000w, <?php echo $headshot['sizes']['xlarge']; ?> 1200w"  alt="<?php echo $headshot['alt']; ?>">
										<?php endif; ?>
										<p><?php the_sub_field('author_title'); ?></p>
									</div>
								<?php endif; ?>
								<?php if ( get_sub_field('button') ) : ?>
									<?php 
										$link = get_sub_field('button'); 
										$link_url = $link['url'];
										$link_title = $link['title'];
										$link_target = $link['target'] ? $link['target'] : '_self'; 
									?>
									<a class="button <?php the_sub_field('color'); ?> <?php if ( get_sub_field('icon') ) : ?>has-icon<?php endif; ?>" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>">
										<?php if ( get_sub_field('icon') ) : ?>
											<?php $icon = get_sub_field('icon'); ?>
											<img class="lazyload blur-up" data-expand="250" data-sizes="auto" src="<?php echo $icon['sizes']['placeholder']; ?>" data-src="<?php echo $icon['sizes']['large']; ?>" data-srcset="<?php echo $icon['sizes']['small']; ?> 350w, <?php echo $icon['sizes']['medium']; ?> 700w, <?php echo $icon['sizes']['large']; ?> 1000w, <?php echo $icon['sizes']['xlarge']; ?> 1200w"  alt="<?php echo $icon['alt']; ?>">
										<?php endif; ?>
										<span><?php echo esc_html($link_title); ?></span>
									</a>
								<?php endif; ?>
							</article>
						<?php endif; ?>
					<?php endwhile; ?>
				</div>
			<?php endwhile; ?>
		</div>
	<?php endif; ?>
</section>
