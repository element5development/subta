<?php 
/*----------------------------------------------------------------*\

	ARTICLE SECTION
	displaying 2 columns one with a image the other with a icon list

\*----------------------------------------------------------------*/
?>

<?php
	$image = get_sub_field('image');
?>

<section id="section-<?php echo $template_args['sectionId']; ?>" class="icon-list is-full-width <?php the_sub_field('image_alignment'); ?>">
	<div class="graphic">
		<img class="lazyload blur-up" data-expand="250" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['large']; ?>" data-srcset="<?php echo $image['sizes']['small']; ?> 300w, <?php echo $image['sizes']['medium']; ?> 700w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1200w"  alt="<?php echo $image['alt']; ?>">
	</div>
	<div class="list">
		<?php if ( get_sub_field('title') ) : ?>
			<h2><?php the_sub_field('title'); ?></h2>
		<?php endif; ?>
		<ul>
			<?php while( have_rows('icon_list') ) : the_row(); ?>
				<?php $icon = get_sub_field('icon'); ?>
				<li>
					<img class="lazyload blur-up" data-expand="250" data-sizes="auto" src="<?php echo $icon['sizes']['placeholder']; ?>" data-src="<?php echo $icon['sizes']['large']; ?>" data-srcset="<?php echo $icon['sizes']['small']; ?> 300w, <?php echo $icon['sizes']['medium']; ?> 700w, <?php echo $icon['sizes']['large']; ?> 1000w, <?php echo $icon['sizes']['xlarge']; ?> 1200w"  alt="<?php echo $icon['alt']; ?>">
					<div>
						<?php if ( get_sub_field('label') ) : ?>
							<h4><?php the_sub_field('label'); ?></h4>
						<?php endif; ?>
						<?php if ( get_sub_field('description') ) : ?>
							<p><?php the_sub_field('description'); ?></p>
						<?php endif; ?>
					</div>
				</li>
			<?php endwhile; ?>
		</ul>
	</div>
</section>