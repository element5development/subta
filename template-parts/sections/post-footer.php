<?php /*
FOOTER CONTENT
*/ ?>

<footer id="footer">
	<div class="brand">
		<div class="contact">
			<a href="<?php echo get_site_url(); ?>">
				<svg id="logo">
					<use xlink:href="#logo-white" />
				</svg>
			</a>
		</div>
		<div class="social-links">
			<a target="_blank" href="https://www.facebook.com/SUBSCRIPTIONTRADEASSOCIATION/">
				<svg>
					<use xlink:href="#facebook-white" />
				</svg>
			</a>
			<a target="_blank" href="https://www.instagram.com/subtastudios/">
				<svg>
					<use xlink:href="#instagram-white" />
				</svg>
			</a>
			<a target="_blank" href="https://www.linkedin.com/company/subta">
				<svg>
					<use xlink:href="#linkedin-white" />
				</svg>
			</a>
			<a target="_blank" href="https://www.youtube.com/channel/UCSSrvzhyrt01g3Adx4z98BQ">
				<svg>
					<use xlink:href="#youtube-white" />
				</svg>
			</a>
		</div>
	</div>
	<div class="navigations">
		<?php if ( is_active_sidebar( 'footer-col-one' ) ) : ?>
			<?php dynamic_sidebar( 'footer-col-one' ); ?>
		<?php endif; ?>
		<?php if ( is_active_sidebar( 'footer-col-two' ) ) : ?>
			<?php dynamic_sidebar( 'footer-col-two' ); ?>
		<?php endif; ?>
		<?php if ( is_active_sidebar( 'footer-col-three' ) ) : ?>
			<?php dynamic_sidebar( 'footer-col-three' ); ?>
		<?php endif; ?>
		<?php if ( is_active_sidebar( 'footer-col-four' ) ) : ?>
			<?php dynamic_sidebar( 'footer-col-four' ); ?>
		<?php endif; ?>
		<?php if ( is_active_sidebar( 'footer-col-five' ) ) : ?>
			<?php dynamic_sidebar( 'footer-col-five' ); ?>
		<?php endif; ?>
	</div>
	<div class="legal">
		<p>© <?php echo date('Y'); ?> SUBTA. All Rights Reserved. <span>External links are provided for reference purposes.<br>SUBTA is not responsible for the content of external Internet sites.</span></p>
		<nav><?php wp_nav_menu(array( 'theme_location' => 'legal-nav' )); ?></nav>
	</div>
</footer>