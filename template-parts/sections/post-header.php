<?php 
/*----------------------------------------------------------------*\

	POST HEADER
	Display the post title

\*----------------------------------------------------------------*/
?>

<header class="post-head">
	<div class="is-standard">
		<?php
			if ( function_exists('yoast_breadcrumb') ) :
				yoast_breadcrumb('<nav class="breadcrumbs">','</nav>');
			endif;
		?>
		<h1><?php the_title(); ?></h1>
		<?php if ( get_field('author') && get_field('author_headshot') ) : ?>
			<div class="post-author">
				<?php $image = get_field('author_headshot'); ?>
				<figure>
					<img class="lazyload blur-up" data-expand="250" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['large']; ?>" data-srcset="<?php echo $image['sizes']['small']; ?> 350w, <?php echo $image['sizes']['medium']; ?> 700w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1200w"  alt="<?php echo $image['alt']; ?>">
				</figure>
				<p>Written by <?php the_field('author'); ?></p>
			</div>
		<?php endif; ?>
		<?php if ( get_field('header_description') ) : ?>
			<p><?php the_field('header_description'); ?></p>
		<?php endif; ?>
	</div>
</header>