<?php 
	/*----------------------------------------------------------------*\
	|
	| Insert page content which is most often handled via ACF Pro
	| and highly recommend the use of the flexiable content so
	|	we already placed that code here.
	|
	| https://www.advancedcustomfields.com/resources/flexible-content/
	|
	\*----------------------------------------------------------------*/
?>	
<?php
	$id = 0;
	while ( have_rows('article') ) : the_row();
		$id++;
		if( get_row_layout() == 'editor' ):
			hm_get_template_part( 'template-parts/sections/article/editor', [ 'sectionId' => $id ] );
		elseif( get_row_layout() == '2editor' ):
			hm_get_template_part( 'template-parts/sections/article/editor-2-column', [ 'sectionId' => $id ] );
		elseif( get_row_layout() == '3editor' ):
			hm_get_template_part( 'template-parts/sections/article/editor-3-column', [ 'sectionId' => $id ] );
		elseif( get_row_layout() == 'responsive_video' ):
			hm_get_template_part( 'template-parts/sections/article/responsive-video', [ 'sectionId' => $id ] );
		elseif( get_row_layout() == 'media+text' ):
			hm_get_template_part( 'template-parts/sections/article/media-text', [ 'sectionId' => $id ] );
		elseif( get_row_layout() == 'profile+text' ):
			hm_get_template_part( 'template-parts/sections/article/profile-text', [ 'sectionId' => $id ] );
		elseif( get_row_layout() == 'price+text' ):
			hm_get_template_part( 'template-parts/sections/article/price-text', [ 'sectionId' => $id ] );
		elseif( get_row_layout() == 'icon_list' ):
			hm_get_template_part( 'template-parts/sections/article/icon-list', [ 'sectionId' => $id ] );
		elseif( get_row_layout() == 'gallery' ):
			hm_get_template_part( 'template-parts/sections/article/gallery', [ 'sectionId' => $id ] );
		elseif( get_row_layout() == 'cover' ):
			hm_get_template_part( 'template-parts/sections/article/cover', [ 'sectionId' => $id ] );
		elseif( get_row_layout() == 'logo_grid' ):
			hm_get_template_part( 'template-parts/sections/article/logo-grid', [ 'sectionId' => $id ] );
		elseif( get_row_layout() == 'testimonials' ):
			hm_get_template_part( 'template-parts/sections/article/testimonials', [ 'sectionId' => $id ] );
		elseif( get_row_layout() == 'featured_content' ):
			hm_get_template_part( 'template-parts/sections/article/featured-content', [ 'sectionId' => $id ] );
		elseif( get_row_layout() == 'post_grid' ):
			hm_get_template_part( 'template-parts/sections/article/post-grid', [ 'sectionId' => $id ] );
		elseif( get_row_layout() == 'price+cards' ):
			hm_get_template_part( 'template-parts/sections/article/price-cards', [ 'sectionId' => $id ] );
		elseif( get_row_layout() == 'card_grid' ):
			hm_get_template_part( 'template-parts/sections/article/card-grid', [ 'sectionId' => $id ] );
		elseif( get_row_layout() == 'newsletter' ):
			hm_get_template_part( 'template-parts/sections/article/newsletter', [ 'sectionId' => $id ] );
		endif;
	endwhile;
?>
<?php if ( !empty( get_the_content() ) ) : ?>
	<section class="is-standard">
		<?php the_content(); ?>
	</section>
<?php endif; ?>