<?php /*
MAIN NAVIGATION
*/ ?>

<div class="primary-nav-wrap">
	<a class="logo" href="<?php echo get_home_url(); ?>">
		<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/subta-logo-full.svg" alt="SUBTA Subscription Trade Association Logo" />
	</a>
	<nav class="primary-nav">
		<?php wp_nav_menu(array( 'theme_location' => 'primary-nav' )); ?>
		<?php if ( !is_user_logged_in() ) : ?>
			<a href="/join" class="button is-yellow">Join</a>
		<?php endif; ?>
		<?php echo do_shortcode('[miniorange_ym_sso]'); ?>
		<a href="/?s" class="is-icon icon-search">Search</a>
		<?php if ( is_user_logged_in() ) : ?>
			<a href="https://subta.site-ym.com/members/dashboard.aspx" class="is-icon icon-account">Account</a>
		<?php endif; ?>
	</nav>
	<nav class="mobile-nav">
		<a href="<?php the_permalink(398); ?>" class="button is-yellow">Join SUBTA</a>
		<button class="open-menu">
			<svg>
				<use xlink:href="#menu" />
			</svg>
		</button>
		<div class="mobile-menu-container">
			<button class="close-menu">
				<svg>
				<use xlink:href="#close-x" />
				</svg>
			</button>
			<?php // echo get_search_form(); ?>
			<?php wp_nav_menu(array( 'theme_location' => 'primary-nav' )); ?>
			<?php if ( !is_user_logged_in() ) : ?>
				<a href="/join" class="join">Join</a>
			<?php endif; ?>
			<?php echo do_shortcode('[miniorange_ym_sso]'); ?>
				<a href="/?s" class="is-icon icon-search">Search</a>
			<?php if ( is_user_logged_in() ) : ?>
				<a href="https://subta.site-ym.com/members/dashboard.aspx" class="is-icon icon-account">Account</a>
			<?php endif; ?>
		</div>
	</nav>
</div>