<?php /*
	Partner's Archive
*/ ?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<header class="post-head is-standard">
	<?php if ( get_field('partner_title','options') ) : ?>
		<h1><?php the_field('partner_title','options'); ?></h1>
	<?php else : ?>
		<h1>Resource Partners</h1>
	<?php endif; ?>
	<?php if ( get_field('partner_description','options') ) : ?>
		<p><?php the_field('partner_description','options'); ?></p>
	<?php endif; ?>
</header>

<main id="main-content" class="full-width">
	<!-- FILTER -->
	<section class="partner-filter is-full-width">
		<div class="is-standard">
			<?php 
				$industries = get_terms( array(
					'taxonomy' => 'specialization',
					'orderby' => 'name',
					'hide_empty' => true
				) );
			?>
			<?php if ( !empty($industries) ) : ?>
				<div class="filter">
					<span>Who specialize in</span>
					<button></button>
				</div>
				<div class="industries">
					<?php foreach( $industries as $industry ) : ?>
						<a href="<?php echo get_term_link($industry->term_id); ?>" class="industry"><?php echo $industry->name; ?><span><?php echo $industry->count; ?></span></a>
					<?php endforeach; ?>
				</div>
			<?php endif; ?>
		</div>
	</section>
	<!-- PREMIUM PARTNERS -->
	<?php 
		$args = array(
			'posts_per_page'	=> -1,
			'post_type'			=> 'sponsors',
			'meta_key'		=> 'premium',
			'meta_value'	=> '1',
			'orderby' => 'rand',
    	'order'    => 'ASC'
		);
		$premium_query = new WP_Query( $args );
	?>
	<?php if( $premium_query->have_posts() ) : ?>
		<section class="partner-grid premium-partners is-extra-wide">
			<h2>Premium Partners</h2>
			<ul>
				<?php while( $premium_query->have_posts() ) : $premium_query->the_post(); ?>
					<li>
						<a href="<?php the_permalink(); ?>">
							<?php if ( get_field('logo') ) : ?>
								<?php $image = get_field('logo'); ?>
								<img class="logo lazyload blur-up" data-expand="250" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['large']; ?>" data-srcset="<?php echo $image['sizes']['small']; ?> 300w, <?php echo $image['sizes']['medium']; ?> 700w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1200w"  alt="<?php echo $image['alt']; ?>">
							<?php else : ?>
								<?php the_title(); ?>
							<?php endif; ?>
						</a>
					</li>
				<?php endwhile; ?>
			</ul>
		</section>
	<?php endif; wp_reset_query(); ?>
	<!-- CALL TO ACTION -->
	<?php if ( get_field('partner_cta_button','options') ) : ?>
		<section class="partner-cta is-narrow">
			<?php if ( get_field('partner_cta_title','options') ) : ?>
				<h2><?php the_field('partner_cta_title','options'); ?></h2>
			<?php endif; ?>
			<?php if ( get_field('partner_cta_description','options') ) : ?>
				<p><?php the_field('partner_cta_description','options'); ?></p>
			<?php endif; ?>
			<?php 
				$link = get_field('partner_cta_button','options');
				$link_url = $link['url'];
				$link_title = $link['title'];
				$link_target = $link['target'] ? $link['target'] : '_self';
			?>
			<a class="button is-blue" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a>
		</section>
	<?php endif; ?>
	<!-- STANDARD PARTNERS -->
	<?php 
		$args = array(
			'posts_per_page'	=> -1,
			'post_type'			=> 'sponsors',
			'meta_key'		=> 'premium',
			'meta_value'	=> '0',
			'orderby' => 'rand',
    	'order'    => 'ASC'
		);
		$standard_query = new WP_Query( $args );
	?>
	<?php if( $standard_query->have_posts() ) : ?>
		<section class="partner-grid standard-partners is-extra-wide">
			<h2>Standard Partners</h2>
			<ul>
				<?php while( $standard_query->have_posts() ) : $standard_query->the_post(); ?>
					<li>
						<a href="<?php the_permalink(); ?>">
							<?php if ( get_field('logo') ) : ?>
								<?php $image = get_field('logo'); ?>
								<img class="logo lazyload blur-up" data-expand="250" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['large']; ?>" data-srcset="<?php echo $image['sizes']['small']; ?> 300w, <?php echo $image['sizes']['medium']; ?> 700w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1200w"  alt="<?php echo $image['alt']; ?>">
							<?php else : ?>
								<?php the_title(); ?>
							<?php endif; ?>
						</a>
					</li>
				<?php endwhile; ?>
			</ul>
		</section>
	<?php endif; wp_reset_query(); ?>
</main>

<?php get_template_part('template-parts/sections/post-footer'); ?>

<?php get_footer(); ?>
