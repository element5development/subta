<?php
/*----------------------------------------------------------------*\
	Register Taxonomy Specialization
\*----------------------------------------------------------------*/
function create_specialization_tax() {
	$labels = array(
		'name'              => _x( 'Specializations', 'taxonomy general name', 'textdomain' ),
		'singular_name'     => _x( 'Specialization', 'taxonomy singular name', 'textdomain' ),
		'search_items'      => __( 'Search Specializations', 'textdomain' ),
		'all_items'         => __( 'All Specializations', 'textdomain' ),
		'parent_item'       => __( 'Parent Specialization', 'textdomain' ),
		'parent_item_colon' => __( 'Parent Specialization:', 'textdomain' ),
		'edit_item'         => __( 'Edit Specialization', 'textdomain' ),
		'update_item'       => __( 'Update Specialization', 'textdomain' ),
		'add_new_item'      => __( 'Add New Specialization', 'textdomain' ),
		'new_item_name'     => __( 'New Specialization Name', 'textdomain' ),
		'menu_name'         => __( 'Specialization', 'textdomain' ),
	);
	$args = array(
		'labels' => $labels,
		'description' => __( '', 'textdomain' ),
		'hierarchical' => true,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'show_in_nav_menus' => true,
		'show_tagcloud' => true,
		'show_in_quick_edit' => true,
		'show_admin_column' => false,
		'show_in_rest' => true,
	);
	register_taxonomy( 'specialization', array('sponsors'), $args );
}
add_action( 'init', 'create_specialization_tax' );
/*----------------------------------------------------------------*\
	Register Taxonomy Post Series
\*----------------------------------------------------------------*/
function create_postseries_tax() {
	$labels = array(
		'name'              => _x( 'Post Series', 'taxonomy general name', 'textdomain' ),
		'singular_name'     => _x( 'Post Series', 'taxonomy singular name', 'textdomain' ),
		'search_items'      => __( 'Search Post Series', 'textdomain' ),
		'all_items'         => __( 'All Post Series', 'textdomain' ),
		'parent_item'       => __( 'Parent Post Series', 'textdomain' ),
		'parent_item_colon' => __( 'Parent Post Series:', 'textdomain' ),
		'edit_item'         => __( 'Edit Post Series', 'textdomain' ),
		'update_item'       => __( 'Update Post Series', 'textdomain' ),
		'add_new_item'      => __( 'Add New Post Series', 'textdomain' ),
		'new_item_name'     => __( 'New Post Series Name', 'textdomain' ),
		'menu_name'         => __( 'Post Series', 'textdomain' ),
	);
	$args = array(
		'labels' => $labels,
		'description' => __( '', 'textdomain' ),
		'hierarchical' => false,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'show_in_nav_menus' => true,
		'show_tagcloud' => true,
		'show_in_quick_edit' => true,
		'show_admin_column' => false,
		'show_in_rest' => true,
		// Control the slugs used for this taxonomy
    'rewrite' => array(
      'slug' => 'series', 
    ),
	);
	register_taxonomy( 'postseries', array('post'), $args );
}
add_action( 'init', 'create_postseries_tax' );
/*----------------------------------------------------------------*\
	Register Taxonomy Show
\*----------------------------------------------------------------*/
function create_show_tax() {
	$labels = array(
		'name'              => _x( 'Shows', 'taxonomy general name', 'textdomain' ),
		'singular_name'     => _x( 'Show', 'taxonomy singular name', 'textdomain' ),
		'search_items'      => __( 'Search Shows', 'textdomain' ),
		'all_items'         => __( 'All Shows', 'textdomain' ),
		'parent_item'       => __( 'Parent Show', 'textdomain' ),
		'parent_item_colon' => __( 'Parent Show:', 'textdomain' ),
		'edit_item'         => __( 'Edit Show', 'textdomain' ),
		'update_item'       => __( 'Update Show', 'textdomain' ),
		'add_new_item'      => __( 'Add New Show', 'textdomain' ),
		'new_item_name'     => __( 'New Show Name', 'textdomain' ),
		'menu_name'         => __( 'Show', 'textdomain' ),
	);
	$args = array(
		'labels' => $labels,
		'description' => __( '', 'textdomain' ),
		'hierarchical' => false,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'show_in_nav_menus' => true,
		'show_tagcloud' => false,
		'show_in_quick_edit' => true,
		'show_admin_column' => false,
		'show_in_rest' => true,
	);
	register_taxonomy( 'show', array('video'), $args );
}
add_action( 'init', 'create_show_tax' );
/*----------------------------------------------------------------*\
	Register Taxonomy Topic
\*----------------------------------------------------------------*/
function create_topic_tax() {
	$labels = array(
		'name'              => _x( 'Topics', 'taxonomy general name', 'textdomain' ),
		'singular_name'     => _x( 'Topic', 'taxonomy singular name', 'textdomain' ),
		'search_items'      => __( 'Search Topics', 'textdomain' ),
		'all_items'         => __( 'All Topics', 'textdomain' ),
		'parent_item'       => __( 'Parent Topic', 'textdomain' ),
		'parent_item_colon' => __( 'Parent Topic:', 'textdomain' ),
		'edit_item'         => __( 'Edit Topic', 'textdomain' ),
		'update_item'       => __( 'Update Topic', 'textdomain' ),
		'add_new_item'      => __( 'Add New Topic', 'textdomain' ),
		'new_item_name'     => __( 'New Topic Name', 'textdomain' ),
		'menu_name'         => __( 'Topic', 'textdomain' ),
	);
	$args = array(
		'labels' => $labels,
		'description' => __( '', 'textdomain' ),
		'hierarchical' => false,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'show_in_nav_menus' => true,
		'show_tagcloud' => false,
		'show_in_quick_edit' => true,
		'show_admin_column' => false,
		'show_in_rest' => true,
	);
	register_taxonomy( 'topic', array('video'), $args );
}
add_action( 'init', 'create_topic_tax' );