<?php
/*----------------------------------------------------------------*\
	WOOCOMMERCE SUPPORT
\*----------------------------------------------------------------*/
add_action( 'after_setup_theme', 'woocommerce_support' );
function woocommerce_support() {
		add_theme_support( 'woocommerce' );
}
//AUTO FULLFILL ORDERS (https://docs.woocommerce.com/document/automatically-complete-orders/)
add_action( 'woocommerce_thankyou', 'custom_woocommerce_auto_complete_order' );
function custom_woocommerce_auto_complete_order( $order_id ) { 
		if ( ! $order_id ) {
				return;
		}
		$order = wc_get_order( $order_id );
		$order->update_status( 'completed' );
}
//Change the Shipping Address checkout label
function wc_shipping_field_strings( $translated_text, $text, $domain ) {
  switch ( $translated_text ) {
  case 'Ship to a different address?' :
  $translated_text = __( 'Where should we ship your Welcome Box?', 'woocommerce' );
  break;
  }
  return $translated_text;
  }
  add_filter( 'gettext', 'wc_shipping_field_strings', 20, 3 );

  function bbloomer_redirect_checkout_add_cart( $url ) {
    $url = get_permalink( get_option( 'woocommerce_checkout_page_id' ) ); 
    return $url;
  }
  add_filter( 'woocommerce_add_to_cart_redirect', 'bbloomer_redirect_checkout_add_cart' );

  add_action( 'wp_enqueue_scripts', function() {
    wp_dequeue_style( 'select2' );
    wp_dequeue_script( 'select2');
    wp_dequeue_script( 'selectWoo' );
}, 11 );
//remove related products from single product page
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );
//remove meta from single product
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );