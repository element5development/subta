<?php
/*----------------------------------------------------------------*\
	INITIALIZE WIDGET AREA
\*----------------------------------------------------------------*/
function custom_sidebars() {
	$args = array(
		'name'          => __( 'Footer Column One', 'textdomain' ),
		'id'            => 'footer-col-one',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h4>',
		'after_title'   => '</h4>',
	);
	register_sidebar($args);
	$args = array(
		'name'          => __( 'Footer Column Two', 'textdomain' ),
		'id'            => 'footer-col-two',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h4>',
		'after_title'   => '</h4>',
	);
	register_sidebar($args);
	$args = array(
		'name'          => __( 'Footer Column Three', 'textdomain' ),
		'id'            => 'footer-col-three',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h4>',
		'after_title'   => ' </h4>',
	);
	register_sidebar($args);
	$args = array(
		'name'          => __( 'Footer Column Four', 'textdomain' ),
		'id'            => 'footer-col-four',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h4>',
		'after_title'   => ' </h4>',
	);
	register_sidebar($args);
	$args = array(
		'name'          => __( 'Footer Column Five', 'textdomain' ),
		'id'            => 'footer-col-five',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h4>',
		'after_title'   => ' </h4>',
	);
	register_sidebar($args);
}
add_action( 'widgets_init', 'custom_sidebars' );