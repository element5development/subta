<?php
/*----------------------------------------------------------------*\
	GAVITY FORM TAB INDEX FIX
\*----------------------------------------------------------------*/
add_filter("gform_tabindex", function () {
	return false;
});

/*----------------------------------------------------------------*\
	ENABLE LABEL VISABILITY
\*----------------------------------------------------------------*/
add_filter( 'gform_enable_field_label_visibility_settings', '__return_true' );

/*----------------------------------------------------------------*\
	CHANGE SUBMIT INPUTS TO BUTTONS
\*----------------------------------------------------------------*/
function form_submit_button ( $button, $form ){
  $button = str_replace( 'input', 'button class="button is-green"', $button );
  $button = str_replace( "/", "", $button );
  $button .= "{$form['button']['text']}</button>";
  return $button;
}
add_filter( 'gform_submit_button', 'form_submit_button', 10, 5 );
/*----------------------------------------------------------------*\
	SUBSUMMIT 2021 ATTENDEE ACCESS COOKIE
\*----------------------------------------------------------------*/
add_action( 'gform_after_submission_94', 'access_cookie', 10, 2 );
function access_cookie( $entry, $form ) {
    // Set a third parameter to specify a cookie expiration time, 
    // otherwise it will last until the end of the current session.
		$expire =  time()+60*60*12; //12 hours
    setcookie( 'subsummit_2021_access', 'true', $expire, "/" );
}