<?php
/*----------------------------------------------------------------*\
	OPTION PAGES
\*----------------------------------------------------------------*/
?>
<?php
	if( function_exists('acf_add_options_page') ) {
		acf_add_options_page(array(
			'page_title' 	=> 'Archives',
			'menu_title'	=> 'Archives',
			'menu_slug' 	=> 'archives',
			'capability'	=> 'edit_posts',
			'redirect'		=> false
		));
	}
	if( function_exists('acf_add_options_page') ) {
		acf_add_options_page(array(
			'page_title' 	=> 'Live Discussion',
			'menu_title'	=> 'Live Discussions',
			'menu_slug' 	=> 'live-discussion',
			'capability'	=> 'edit_posts',
			'redirect'		=> false
		));
	}
	if( function_exists('acf_add_options_page') ) {
		acf_add_options_page(array(
			'page_title' 	=> 'Gated CTAs',
			'menu_title'	=> 'Gated CTAs',
			'menu_slug' 	=> 'gated',
			'capability'	=> 'edit_posts',
			'redirect'		=> false
		));
	}
?>