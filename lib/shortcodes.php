<?php
/*----------------------------------------------------------------*\
	INITIALIZE BUTTON SHORTCODE
\*----------------------------------------------------------------*/
// BUTTONS
function button_shortcode($atts, $content = null) {
	$atts = shortcode_atts(
		array(
			'size' => 'normal',
			'color' => 'black',
			'target' => '_self',
			'url' => '#',
		),
		$atts,
		'button'
	);
	$size = $atts['size'];
	$color = $atts['color'];
	$target = $atts['target'];
	$url = $atts['url'];

	$link = '<a target="'.$target.'" href="'.$url.'" class="button is-'.$color.' is-'.$size.'">' . do_shortcode($content) . '</a>';
  return $link;
}
add_shortcode( 'button', 'button_shortcode' );
// LEAGAL TEXT SHORTCODE
function legal_text($atts, $content = null) {
	extract( shortcode_atts( array( ), $atts ) );
	return '<p class="legal-text">' . do_shortcode($content) . '</p>';
}
add_shortcode('legal-text', 'legal_text');