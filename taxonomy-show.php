<?php 
/*----------------------------------------------------------------*\

	SHOW TAXONOMY FOR VIDEO

\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php //IF ACCESSING PAGE WITHOUT LOGIN FORM
	if ( is_tax('show', 'subsummit-2021') && !isset($_COOKIE['subsummit_2021_access']) ) :
		header('Location: https://subta.com/access-subsummit-2021/');
		exit;
	endif; 
?>

<?php $term = get_queried_object(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<!-- PAGE TITLE AND BANNER -->
<?php $background = get_field('cover', $term); ?>
<header class="post-head" style="background-image: url('<?php echo $background['sizes']['xlarge']; ?>');">
	<div class="is-extra-wide">
		<div>
			<?php if ( get_field('logo', $term) ) : ?>
				<?php $image = get_field('logo', $term); ?>
				<img class="lazyload" data-expand="250" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['large']; ?>" data-srcset="<?php echo $image['sizes']['small']; ?> 300w, <?php echo $image['sizes']['medium']; ?> 700w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1200w"  alt="<?php echo $image['alt']; ?>">
			<?php endif; ?>
			<h1><?php single_cat_title() ?></h1>
			<?php echo category_description(); ?>
			<?php 
				$the_query = new WP_Query( array(
					'post_type' => 'video',
					'posts_per_page' => 1,
					'orderby' => 'menu_order',
					'order' => 'ASC',
					'tax_query' => array(
						array (
							'taxonomy' => 'show',
							'field' => 'slug',
							'terms' => get_queried_object()->slug,
						)
					),
				));	
			?>
			<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
				<a href="<?php echo get_permalink(); ?>" class="button is-green">Watch the latest episode</a>
			<?php endwhile; wp_reset_query(); ?>
		</div>
	</div>
</header>

<main id="main-content" class="full-width">
	<article>

		<?php 
			$the_query = new WP_Query( array(
				'post_type' => 'video',
				'posts_per_page' => 1,
				'orderby' => 'menu_order',
				'order' => 'ASC',
				'tax_query' => array(
					array (
						'taxonomy' => 'show',
						'field' => 'slug',
						'terms' => get_queried_object()->slug,
					)
				),
			));	
		?>
		<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
			<section class="latest-video video-post-preview is-extra-wide">
				<a href="<?php echo get_permalink(); ?>">
					<div class="image-wrap">
						<?php if ( get_field('video_thumbnail') ) : ?>
							<?php $image = get_field('video_thumbnail'); ?>
							<img class="lazyload" data-expand="250" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['large']; ?>" data-srcset="<?php echo $image['sizes']['small']; ?> 300w, <?php echo $image['sizes']['medium']; ?> 700w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1200w"  alt="<?php echo $image['alt']; ?>">
						<?php else : ?>
							<img class="lazyload" data-expand="250" data-sizes="auto" src="/wp-content/themes/subta/dist/images/default-post-graphic.png"  alt="SUBTA -">
						<?php endif; ?>
						<?php if ( get_field('access_level') != 'public' ) : ?>
							<div class="gated">
								<svg>
									<use xlink:href="#gated" />
								</svg>
								<span>Member Only</span>
							</div>
						<?php endif; ?>
					</div>
					<div class="contents">
						<p class="meta">
							<span>Latest Episode</span>
							<?php if ( get_field('run_time') ) : ?>
								<span><?php the_field('run_time'); ?> min</span>
							<?php endif; ?>
							<?php echo get_the_date('F j, Y'); ?>
						</p>
						<h2><?php echo get_the_title(); ?></h2>
						<p><?php the_field('post_header_description'); ?></p>
						<!-- <?php if ( get_field('guest') ) : ?>
							<p class="meta">Featuring</p>
							<div class="guests is-extra-wide">
								<?php while( have_rows('guest') ) : the_row(); ?>
									<div class="guest">
										<?php $image = get_sub_field('headshot'); ?>
										<img class="lazyload" data-expand="250" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['large']; ?>" data-srcset="<?php echo $image['sizes']['small']; ?> 300w, <?php echo $image['sizes']['medium']; ?> 700w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1200w"  alt="<?php echo $image['alt']; ?>" title="<?php the_sub_field('name'); ?>">
									</div>
								<?php endwhile; ?>
							</div>
						<?php endif; ?> -->
					</div>
				</a>
			</section>
		<?php endwhile; ?>

		<section class="video-feed is-extra-wide">
			<h2>Past Episodes</h2>
			<?php //while ( $wp_query->have_posts() ) : $wp_query->the_post(); ?>
			<?php	while ( have_posts() ) : the_post(); ?>
				<article class="video-post-preview">
					<a href="<?php echo get_permalink(); ?>">
						<?php if ( get_field('video_thumbnail') ) : ?>
							<?php $image = get_field('video_thumbnail'); ?>
							<img class="lazyload" data-expand="250" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['large']; ?>" data-srcset="<?php echo $image['sizes']['small']; ?> 300w, <?php echo $image['sizes']['medium']; ?> 700w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1200w"  alt="<?php echo $image['alt']; ?>">
						<?php else : ?>
							<img class="lazyload" data-expand="250" data-sizes="auto" src="/wp-content/themes/subta/dist/images/default-post-graphic.png"  alt="SUBTA -">
						<?php endif; ?>
						<?php if ( get_field('access_level') != 'public' ) : ?>
							<div class="gated">
								<svg>
									<use xlink:href="#gated" />
								</svg>
								<span>Member Only</span>
							</div>
						<?php endif; ?>
						<h3><?php echo get_the_title(); ?></h3>
						<p class="meta">
							<?php if ( get_field('run_time') ) : ?>
								<span><?php the_field('run_time'); ?> min</span>
							<?php endif; ?>
							<?php echo get_the_date('F j, Y'); ?>
						</p>
					</a>
				</article>
			<?php endwhile; ?>
		</section>
		<?php clean_pagination(); ?>
		<?php wp_reset_postdata(); ?>
		
	</article>
</main>

<aside class="sub-or-join is-extra-wide">
	<div class="newsletter">
		<h2>Direct to Your Inbox</h2>
		<p>Be notified via email with the latest articles, industry news, partner resources and much more.</p>
		<?php echo do_shortcode('[gravityform id="3" title="false" description="false"]'); ?>
	</div>
	<div class="join">
		<h2>Unlock Everything</h2>
		<p>SUBTA is an ever-growing community of innovators, entrepreneurs, thought leaders and dedicated teams that are eager to scale their businesses and catalyze the subscription industry. Sound like you?</p>	
		<a href="/join" class="button is-yellow">Join SUBTA</a>
	</div>
</aside>

<?php get_template_part('template-parts/sections/post-footer'); ?>

<?php get_footer(); ?>