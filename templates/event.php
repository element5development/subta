<?php /*
Template Name: Event Landing
*/ ?>

<?php get_header(); ?>

<header>
	<?php $image = get_field('subta_logo'); ?>
	<a href="<?php echo get_home_url(); ?>">
		<img class="subta" src="<?php echo esc_url($image['sizes']['small']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
	</a>
	<?php $image = get_field('event_logo'); ?>
	<img class="event-logo" src="<?php echo esc_url($image['sizes']['large']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
	<h1><?php the_field('event_description'); ?></h1>
	<div class="date-details">
		<p><?php the_field('event_time'); ?></p>
		<p><?php the_field('event_date'); ?></p>
	</div>
	<?php $link = get_field('register_button'); ?>
	<?php 
		$link_url = $link['url'];
		$link_title = $link['title'];
		$link_target = $link['target'] ? $link['target'] : '_self';
	?>
	<a class="button register" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>">
		<?php echo esc_html( $link_title ); ?>
		<span><?php the_field('button_label'); ?></span>
	</a>
	<nav>
		<?php if( have_rows('expectation_cards') ): ?>
			<a href="#expectations">What to Expect</a>
		<?php endif; ?>
		<?php if ( get_field('speakers') ) : ?>
			<a href="#speakers">Speakers</a>
		<?php endif; ?>
		<?php if( have_rows('schedule') ): ?>
			<a href="#schedule">Schedule</a>
		<?php endif; ?>
		<!--
			<?php if( get_field('pricing') ): ?>
				<a href="#pricing">Pricing</a>
			<?php endif; ?> 
		-->
		<?php if ( get_field('sponsors') || get_field('sponsors_2') || get_field('sponsors_3') ) : ?>
			<a href="#sponsors">Sponsors</a>
		<?php endif; ?>
	</nav>
</header>

<?php if( get_field('trailer') ): ?>
	<section class="trailer">
		<?php the_field('trailer'); ?>
	</section>
<?php endif; ?>

<?php if( have_rows('expectation_cards') ): ?>
	<section id="expectations" class="expectations">
		<div class="intro">
			<h2><?php the_field('expectations_title'); ?></h2>
			<p><?php the_field('expectations_description'); ?></p>
		</div>
		<div class="expectations-grid">
			<?php while ( have_rows('expectation_cards') ) : the_row(); ?>
				<div class="expectation">
					<?php $image = get_sub_field('image'); ?>
					<img src="<?php echo esc_url($image['sizes']['medium']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
					<h3><?php the_sub_field('title'); ?></h3>
					<p><?php the_sub_field('description'); ?></p>
				</div>
			<?php endwhile; ?>
		</div>
	</section>
<?php endif; ?>

<?php if( have_rows('speakers') ): ?>
	<section id="speakers" class="speakers">
		<h2><?php the_field('speakers_title'); ?></h2>
		<div class="speakers-grid">
			<?php while ( have_rows('speakers') ) : the_row(); ?>
				<div class="speaker">
					<?php $image = get_sub_field('headshot'); ?>
					<img src="<?php echo esc_url($image['sizes']['medium']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
					<div>
						<?php 
							// $first = strtok( get_sub_field('name'), " " );
							// $last = array_pop( explode(' ', get_sub_field('name') ) );
						?>
						<h3><?php the_sub_field('name'); ?></h3>
						<p><?php the_sub_field('title'); ?> at<br/><?php the_sub_field('company'); ?></p>
					</div>
				</div>
			<?php endwhile; ?>
		</div>
	</section>
<?php endif; ?>

<?php if( have_rows('schedule') ): ?>
	<section id="schedule" class="schedule <?php if ( get_field('track2_title') ): ?>two-tracks<?php endif; ?> <?php if ( get_field('hide_time') ): ?>time-hidden<?php endif; ?>">
		<h2><?php the_field('schedule_title'); ?></h2>
		<div class="schedule-grid">
			<?php if ( get_field('track_title') || get_field('track2_title') ) : ?>
				<div class="item labels">
					<?php if ( !get_field('hide_time') ) : ?>
						<div class="time"></div>
					<?php endif; ?>
					<?php if ( get_field('track_title') ): ?>
						<div class="description track1">
							<h3><?php the_field('track_title'); ?></h3>
							<?php if ( get_field('track_description') ): ?>
								<p><?php the_field('track_description'); ?></p>
							<?php endif; ?>
						</div>
					<?php endif; ?>
					<?php if ( get_field('track2_title') ): ?>
						<div class="description track2">
							<h3><?php the_field('track2_title'); ?></h3>
							<?php if ( get_field('track2_description') ): ?>
								<p><?php the_field('track2_description'); ?></p>
							<?php endif; ?>
						</div>
					<?php endif; ?>
				</div>
			<?php endif; ?>
			<?php while ( have_rows('schedule') ) : the_row(); ?>
				<div class="item">
					<?php if ( get_sub_field('time') && !get_field('hide_time') ): ?>
						<div class="time"><?php the_sub_field('time'); ?></div>
					<?php endif; ?>
					<div class="description">
						<?php if ( get_sub_field('title') ): ?>
							<h3>
								<span><?php the_field('track_title'); ?></span>
								<?php the_sub_field('title'); ?>
							</h3>
						<?php endif; ?>
						<?php if ( get_sub_field('description') ): ?>
							<p><?php the_sub_field('description'); ?></p>
						<?php endif; ?>
						<?php if( have_rows('speaker_list') ): ?>
							<ul>
								<?php while ( have_rows('speaker_list') ) : the_row(); ?>
									<li>
										<?php $image = get_sub_field('headshot'); ?>
										<img src="<?php echo esc_url($image['sizes']['small']); ?>" alt="<?php echo esc_attr($alt); ?>" />
										<div>
											<p class="name"><?php the_sub_field('name'); ?></p>
											<p class="company"><?php the_sub_field('company'); ?></p>
										</div>
									</li>
								<?php endwhile; ?>
							</ul>
						<?php endif; ?>
					</div>
					<?php if ( get_field('track2_title') && get_sub_field('title_2') ): ?>
						<div class="description">
							<?php if ( get_sub_field('title_2') ): ?>
								<h3>
									<span><?php the_field('track2_title'); ?></span>
									<?php the_sub_field('title_2'); ?>
								</h3>
							<?php endif; ?>
							<?php if ( get_sub_field('description_2') ): ?>
								<p><?php the_sub_field('description_2'); ?></p>
							<?php endif; ?>
							<?php if( have_rows('speaker_list_2') ): ?>
								<ul>
									<?php while ( have_rows('speaker_list_2') ) : the_row(); ?>
										<li>
											<?php $image = get_sub_field('headshot'); ?>
											<img src="<?php echo esc_url($image['sizes']['small']); ?>" alt="<?php echo esc_attr($alt); ?>" />
											<div>
												<p class="name"><?php the_sub_field('name'); ?></p>
												<p class="company"><?php the_sub_field('company'); ?></p>
											</div>
										</li>
									<?php endwhile; ?>
								</ul>
							<?php endif; ?>
						</div>
					<?php endif; ?>
				</div>
			<?php endwhile; ?>
		</div>
	</section>
<?php endif; ?>

<?php if( get_field('pricing') ): ?>
	<section id="pricing" class="pricing">
		<div>
			<?php the_field('pricing'); ?>
		</div>
	</section>
<?php endif; ?>

<?php 
	$images = get_field('sponsors'); 
	$images2 = get_field('sponsors_2'); 
	$images3 = get_field('sponsors_3'); 
?>
<?php if( $images || $images2 || $images3 ): ?>
	<section id="sponsors" class="sponsors">
		<div class="intro">
			<h2><?php the_field('sponsors_title'); ?></h2>
			<?php if ( get_field('sponsor_brochure') ) : ?>
				<?php $brochure = get_field('sponsor_brochure'); ?>
				<?php 
					$brochure_url = $brochure['url'];
					$brochure_title = $brochure['title'];
					$brochure_target = $brochure['target'] ? $brochure['target'] : '_self';
				?>
				<a class="button" href="<?php echo esc_url( $brochure_url ); ?>" target="<?php echo esc_attr( $brochure_target ); ?>">
					<?php echo esc_html( $brochure_title ); ?>
				</a>
			<?php endif; ?>
		</div>
		<?php if ( $images ) : ?>
			<div class="sponsor-grid tier-1">
				<?php foreach( $images as $image ): ?>
					<img src="<?php echo esc_url($image['sizes']['small']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
				<?php endforeach; ?>
			</div>
		<?php endif; ?>
		<?php if ( $images2 ) : ?>
			<div class="sponsor-grid tier-2">
				<?php foreach( $images2 as $image ): ?>
					<img src="<?php echo esc_url($image['sizes']['small']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
				<?php endforeach; ?>
			</div>
		<?php endif; ?>
		<?php if ( $images3 ) : ?>
			<div class="sponsor-grid tier-3">
				<?php foreach( $images3 as $image ): ?>
					<img src="<?php echo esc_url($image['sizes']['small']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
				<?php endforeach; ?>
			</div>
		<?php endif; ?>
	</section>
<?php endif; ?>


<?php if( get_field('downloads') || get_field('share_title') ): ?>
	<section class="promotions">
		<div class="intro">
			<h2><?php the_field('share_title'); ?></h2>
			<?php the_field('share_description'); ?>
		</div>
		<div class="promotions-grid">
			<?php if ( get_field('downloads') ) : ?>
				<?php $downloads = get_field('downloads'); ?>
				<?php foreach( $downloads as $download ): ?>
					<a target="_blank" href="<?php echo esc_url($download['sizes']['large']); ?>">
						<img src="<?php echo esc_url($download['sizes']['medium']); ?>" alt="<?php echo esc_attr($download['alt']); ?>" />
						<div class="button">download</div>
					</a>
				<?php endforeach; ?>
			<?php endif; ?>
		</div>
	</section>
<?php endif; ?>

<?php $link = get_field('register_button'); ?>
<?php 
	$link_url = $link['url'];
	$link_title = $link['title'];
	$link_target = $link['target'] ? $link['target'] : '_self';
?>
<a class="button register is-fixed" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>">
	Register
</a>

<div class="legal-footer">
	<p>© <?php echo date('Y'); ?> SUBTA Subscription Trade Association. All Rights Reserved.</p>
	<nav><?php wp_nav_menu( array( 'theme_location' => 'legal-nav' ) ); ?></nav>
</div>

<?php if ( is_page('59124') ) : ?>
	<script type="text/javascript">
	  jQuery('a.register').click(function() {
		fbq('track', 'AddToCart');
	  });
	</script>
<?php endif; ?>

<?php get_footer(); ?>