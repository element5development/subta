<?php 
/*----------------------------------------------------------------*\

	Template Name: Front Page

\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/cookie-bar'); ?>

<?php if ( get_field('display_link') && get_field('desktop_ad') && get_field('mobile_ad') ) : ?>
	<aside class="display-ad">
		<a target="_blank" href="<?php the_field('display_link'); ?>">
			<?php 
				$desktopAd = get_field('desktop_ad');
				$mobileAd = get_field('mobile_ad');  
			?>
			<img class="desktop-display-ad lazyload blur-up" data-expand="250" data-sizes="auto" src="<?php echo $desktopAd['sizes']['placeholder']; ?>" data-src="<?php echo $desktopAd['sizes']['large']; ?>" data-srcset="<?php echo $desktopAd['sizes']['small']; ?> 350w, <?php echo $desktopAd['sizes']['medium']; ?> 700w, <?php echo $desktopAd['sizes']['large']; ?> 1000w, <?php echo $desktopAd['sizes']['xlarge']; ?> 1200w"  alt="<?php echo $desktopAd['alt']; ?>">
			<img class="mobile-display-ad lazyload blur-up" data-expand="250" data-sizes="auto" src="<?php echo $mobileAd['sizes']['placeholder']; ?>" data-src="<?php echo $mobileAd['sizes']['large']; ?>" data-srcset="<?php echo $mobileAd['sizes']['small']; ?> 350w, <?php echo $mobileAd['sizes']['medium']; ?> 700w, <?php echo $mobileAd['sizes']['large']; ?> 1000w, <?php echo $mobileAd['sizes']['xlarge']; ?> 1200w"  alt="<?php echo $mobileAd['alt']; ?>">
		</a>
	</aside>
<?php endif; ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<?php if ( get_field('flyout_button') ) : ?>
	<aside class="promo">
		<?php if ( get_field('flyout_title') ) : ?>
			<h4><?php the_field('flyout_title'); ?></h4>
		<?php endif; ?>
		<?php if ( get_field('flyout_description') ) : ?>
			<p><?php the_field('flyout_description'); ?></p>
		<?php endif; ?>
		<?php 
			$link = get_field('flyout_button');
			$link_url = $link['url'];
			$link_title = $link['title'];
			$link_target = $link['target'] ? $link['target'] : '_self';
    ?>
		<a class="button" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a>
		<button>HIDE</button>
	</aside>
<?php endif; ?>

<header class="page-header is-extra-wide">
	<div class="page-title">
		<h1>
			<span>The Subscription Trade Association</span>
			<?php the_title(); ?>
		</h1>
		<?php if( have_rows('segments') ): ?>
			<div class="segments">
				<?php while( have_rows('segments') ) : the_row(); ?>
					<div class="segment">
						<?php 
							$image = get_sub_field('icon'); 
						?>
						<button>
							<img class="lazyload blur-up" data-expand="250" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['large']; ?>" data-srcset="<?php echo $image['sizes']['small']; ?> 350w, <?php echo $image['sizes']['medium']; ?> 700w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1200w"  alt="<?php echo $image['alt']; ?>">
						</button>
						<div class="info">
							<p><b><?php the_sub_field('label'); ?></b></p>
							<p><?php the_sub_field('description'); ?></p>
						</div>
					</div>
				<?php endwhile; ?>
			</div>
		<?php endif; ?>
	</div>
	<div class="statements">
		<?php while( have_rows('subta_statements') ) : the_row(); ?>
			<div class="statement">
				<h2><?php the_sub_field('label'); ?></h2>
				<p><?php the_sub_field('description'); ?></p>
			</div>
		<?php endwhile; ?>
	</div>
</header>

<main id="main-content">
	<?php if ( have_rows('article') || !empty(get_the_content()) ):  ?>
		<article>
			<?php get_template_part('template-parts/article'); ?>
		</article>
	<?php else : ?>
		<article>
			<section class="is-narrow">
				<h2>Uh Oh. Something is missing.</h2>
				<p>Looks like this page has no content.</p>
			</section>
		</article>
	<?php endif; ?>
</main>

<?php get_template_part('template-parts/sections/post-footer'); ?>

<?php get_footer(); ?>