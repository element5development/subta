<?php /*
Template Name: Thank You
*/ ?>

<?php get_header(); ?>

<nav class="simple-nav">
	<a href="<?php echo get_home_url(); ?>">
		<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/subta-logo-full.svg" alt="SUBTA Subscription Trade Association Logo" />
	</a>
	<a class="button is-secondary" href="/">
		Back to Home
	</a>
</nav>
<main id="main-content" class="full-width">
	<article>
		<section>
			<h1><?php the_title(); ?></h1>
			<?php the_content(); ?>
		</section>
	</article>
</main>
<footer id="footer">
	<div>
		<div>
			<a href="<?php echo get_site_url(); ?>">
				<svg id="logo">
					<use xlink:href="#logo-white" />
				</svg>
			</a>
			<address>
				<p>901 Wilshire Dr, Suite 190<br/>Troy, MI 48084</p>
				<a class="email" href="mailto: info@subta.com">
					info@subta.com
				</a>
				<a class="phone" href="tel:+18336378282">
					+1-833-637-8282
				</a>
			</address>
		</div>
		<nav>
			<div class="social-links">
				<a target="_blank" href="https://www.facebook.com/SUBSCRIPTIONTRADEASSOCIATION/">
					<svg>
						<use xlink:href="#facebook" />
					</svg>
				</a>
				<a target="_blank" href="https://www.instagram.com/subtastudios/">
					<svg>
						<use xlink:href="#instagram" />
					</svg>
				</a>
				<a target="_blank" href="https://www.linkedin.com/company/subta">
					<svg>
						<use xlink:href="#linkedin" />
					</svg>
				</a>
				<a target="_blank" href="https://www.youtube.com/channel/UCSSrvzhyrt01g3Adx4z98BQ">
					<svg>
						<use xlink:href="#youtube" />
					</svg>
				</a>
			</div>
		</nav>
	</div>
</footer>
<div class="legal-footer">
	<p>© <?php echo date('Y'); ?> SUBTA Subscription Trade Association. All Rights Reserved.</p>
	<nav><?php wp_nav_menu( array( 'theme_location' => 'legal-nav' ) ); ?></nav>
</div>

<?php get_footer(); ?>