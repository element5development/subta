<?php /*
Template Name: Live Discussions
*/ ?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/cookie-bar'); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<main>
	<article>
	
	<?php $i = 0; while( have_rows('upcoming_episodes', 'options') ) : the_row(); $i++; ?>
		<?php if ( $i < 2 ) : ?>
			<?php $term = get_sub_field('show'); ?>
			<?php $background = get_field('cover', $term); ?>
			<section class="featured-video" style="background-image: url('<?php echo $background['sizes']['xlarge']; ?>');">
				<div class="is-extra-wide">
					<div class="contents">
						<?php if ( get_field('logo', $term) ) : ?>
							<?php $image = get_field('logo', $term); ?>
							<img class="lazyload" data-expand="250" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['large']; ?>" data-srcset="<?php echo $image['sizes']['small']; ?> 300w, <?php echo $image['sizes']['medium']; ?> 700w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1200w"  alt="<?php echo $image['alt']; ?>">
						<?php endif; ?>
						<h2><?php the_sub_field('title'); ?></h2>
						<p><?php the_sub_field('description'); ?></p>
						<a target="_blank" href="<?php the_sub_field('live_stream'); ?>" class="button">Watch <?php the_sub_field('date'); ?> at <?php the_sub_field('time'); ?></a>
					</div>
					<div class="image-wrap">
						<?php if ( get_sub_field('thumbnail') ) : ?>
							<?php $image = get_sub_field('thumbnail'); ?>
							<img class="lazyload" data-expand="250" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['large']; ?>" data-srcset="<?php echo $image['sizes']['small']; ?> 300w, <?php echo $image['sizes']['medium']; ?> 700w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1200w"  alt="<?php echo $image['alt']; ?>">
						<?php endif; ?>
					</div>
				</div>
			</section>
		<?php endif; ?>
	<?php endwhile; ?>

	<?php $count = count( get_field('upcoming_episodes', 'options') ); ?>
	<?php if ( $count > 1 ) : ?>
	<section class="upcoming-episodes video-feed is-extra-wide">
		<?php $i = 0; while( have_rows('upcoming_episodes', 'options') ) : the_row(); $i++; ?>
			<?php if ( $i > 1 ) : ?>
				<article class="video-post-preview">
					<?php if ( get_sub_field('thumbnail') ) : ?>
						<?php $image = get_sub_field('thumbnail'); ?>
						<img class="lazyload" data-expand="250" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['large']; ?>" data-srcset="<?php echo $image['sizes']['small']; ?> 300w, <?php echo $image['sizes']['medium']; ?> 700w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1200w"  alt="<?php echo $image['alt']; ?>">
					<?php endif; ?>
					<h3><?php the_sub_field('title'); ?></h3>
					<p class="meta"><?php the_sub_field('date_time'); ?></p>
					<p><?php the_sub_field('description'); ?></p>
					<a target="_blank" href="<?php the_sub_field('live_stream'); ?>" class="button is-green">Watch <?php the_sub_field('date'); ?></a>
				</article>
			<?php endif; ?>
		<?php endwhile; ?>
	</section>
	<?php endif; ?>

	<?php 
		$terms = get_terms(
			array(
				'taxonomy'   => 'show',
				'hide_empty' => false,
				'meta_query' => array(
					array(
							'key'   => 'live',
							'value' => '1',
					)
				),
			)
		);
	?>
	<?php if ( ! empty( $terms ) && is_array( $terms ) ) : ?>
		<h1 class="is-extra-wide">Live Shows</h1>
		<section class="shows is-extra-wide">
			<?php foreach ( $terms as $term ) : ?>
				<a href="<?php echo esc_url( get_term_link( $term ) ) ?>">
					<article>
						<div class="show-poster">
							<?php if ( get_field('poster', $term) ) : ?>
								<?php $image = get_field('poster', $term); ?>
								<img class="lazyload" data-expand="250" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['large']; ?>" data-srcset="<?php echo $image['sizes']['small']; ?> 300w, <?php echo $image['sizes']['medium']; ?> 700w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1200w"  alt="<?php echo $image['alt']; ?>">
							<?php else : ?>
								<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/show-default-cover.jpg" />
							<?php endif; ?>
						</div>
						<h3><?php echo $term->name; ?></h3>
					</article>
				</a>
			<?php endforeach; ?>
		</section>
	<?php endif; ?>
	</article>
</main>

<?php 
	$terms = get_terms(
		array(
			'taxonomy'   => 'show',
			'hide_empty' => false,
			'meta_query' => array(
				array(
						'key'   => 'live',
						'value' => '1',
				)
				),
		)
	);
	$termArray = array();
	foreach ( $terms as $term ) :
		array_push($termArray, $term->slug);
	endforeach;
	$the_query = new WP_Query( array(
		'post_type' => 'video',
		'posts_per_page' => 12,
		'orderby' => 'publish_date',
		'order' => 'DESC',
		'tax_query' => array(
			array (
					'taxonomy' => 'show',
					'field' => 'slug',
					'terms' => $termArray,
			)
		),
	));	
?>
<aside class="video-feed is-extra-wide">
	<h2>Past Episodes</h2>
	<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
		<article class="video-post-preview">
			<a href="<?php echo get_permalink(); ?>">
				<?php if ( get_field('video_thumbnail') ) : ?>
					<?php $image = get_field('video_thumbnail'); ?>
					<img class="lazyload" data-expand="250" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['large']; ?>" data-srcset="<?php echo $image['sizes']['small']; ?> 300w, <?php echo $image['sizes']['medium']; ?> 700w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1200w"  alt="<?php echo $image['alt']; ?>">
				<?php else : ?>
					<img class="lazyload" data-expand="250" data-sizes="auto" src="/wp-content/themes/subta/dist/images/default-post-graphic.png"  alt="SUBTA -">
				<?php endif; ?>
				<?php if ( get_field('access_level') != 'public' ) : ?>
					<div class="gated">
						<svg>
							<use xlink:href="#gated" />
						</svg>
						<span>Member Only</span>
					</div>
				<?php endif; ?>
				<h3><?php echo get_the_title(); ?></h3>
				<p class="meta">
					<?php if ( get_field('run_time') ) : ?>
						<span><?php the_field('run_time'); ?> min</span>
					<?php endif; ?>
					<?php echo get_the_date('F j, Y'); ?>
				</p>
			</a>
		</article>
	<?php endwhile; ?>
</aside>
<?php clean_pagination(); ?>


<aside class="sub-or-join is-extra-wide">
	<div class="newsletter">
		<h2>Direct to Your Inbox</h2>
		<p>Be notified via email with the latest articles, industry news, partner resources and much more.</p>
		<?php echo do_shortcode('[gravityform id="3" title="false" description="false"]'); ?>
	</div>
	<div class="join">
		<h2>Unlock Everything</h2>
		<p>SUBTA is an ever-growing community of innovators, entrepreneurs, thought leaders and dedicated teams that are eager to scale their businesses and catalyze the subscription industry. Sound like you?</p>	
		<a href="/join" class="button is-yellow">Join SUBTA</a>
	</div>
</aside>

<?php get_template_part('template-parts/sections/post-footer'); ?>

<?php get_footer(); ?>