var $ = jQuery;

$(document).ready(function () {
	/*----------------------------------------------------------------*\
		SELECT FIELD PLACEHOLDER
	\*----------------------------------------------------------------*/
	$(function () {
		$('select').addClass('has-placeholder');
	});
	$("select").change(function () {
		$(this).removeClass('has-placeholder');
	});
	/*----------------------------------------------------------------*\
		FILE UPLOAD
	\*----------------------------------------------------------------*/
	$('label').each(function () {
		if ($(this).siblings('.ginput_container_fileupload').length) {
			$(this).addClass('file-upload-label');
		}
	});

	if ($('input[type="file"]').length > 0) {
		var fileInput = document.querySelector("input[type='file']");
		var button = document.querySelector("input[type='file']+span");
		fileInput.addEventListener("change", function (event) {
			button.innerHTML = this.value;
			$('label.file-upload-label').addClass("file-uploaded");
		});
	}
	/*----------------------------------------------------------------*\
		NOTIFICATION BAR
	\*----------------------------------------------------------------*/
	if (readCookie('cookieNotification') === 'false') {
		$('.cookie-useage-notification').removeClass("note-on");
	} else {
		$('.cookie-useage-notification').addClass("note-on");
	}
	$('.cookie-useage-notification button').click(function () {
		$('.cookie-useage-notification').removeClass("note-on");
		createCookie('cookieNotification', 'false');
	});

	function createCookie(name, value, days) {
		var expires = "";
		if (days) {
			var date = new Date();
			date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
			expires = "; expires=" + date.toUTCString();
		}
		document.cookie = name + "=" + value + expires + "; path=/";
	}

	function readCookie(name) {
		var nameEQ = name + "=";
		var ca = document.cookie.split(';');
		for (var i = 0; i < ca.length; i++) {
			var c = ca[i];
			while (c.charAt(0) == ' ') c = c.substring(1, c.length);
			if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
		}
		return null;
	}
	/*----------------------------------------------------------------*\
		PROMO
	\*----------------------------------------------------------------*/
	if (readCookie('cookiePromo') === 'false') {
		$('.promo').removeClass("note-on");
	} else {
		$('.promo').addClass("note-on");
	}
	$('.promo button').click(function () {
		$('.promo').removeClass("note-on");
		createCookie('cookiePromo', 'false');
	});
	/*----------------------------------------------------------------*\
		ADD CLASS TO IFRAMES
	\*----------------------------------------------------------------*/
	$(function () {
		$('iframe').addClass('lazyload').addClass('blur-up');
	});
	/*----------------------------------------------------------------*\
  	ACTIVATE MENU
	\*----------------------------------------------------------------*/
	$(".open-menu").click(function () {
		$(".mobile-menu-container").addClass("is-active");
		$('html').addClass('no-scroll');
	});
	$(".close-menu").click(function () {
		$(".mobile-menu-container").removeClass("is-active");
		$('html').removeClass('no-scroll');
	});
	$(document).click(function (event) {
		//if you click on anything except within the open menu, close the menu
		if (!$(event.target).closest(".mobile-menu-container, .open-menu").length) {
			$(".mobile-menu-container").removeClass("is-active");
			$('html').removeClass('no-scroll');
		}
	});
	/*----------------------------------------------------------------*\
  	ACTIVATE SEARCH
	\*----------------------------------------------------------------*/
	$('button.activate-search').click(function () {
		$('nav.primary-nav .search-form').addClass('is-active');
		$('nav.primary-nav .search-form form input').focus();
	});
	$(document).click(function (event) {
		//if you click on anything except the search input, search button, or search open button, close the search
		if (!$(event.target).closest("nav.primary-nav .search-form form input,nav.primary-nav .search-form form button,button.activate-search").length) {
			$('nav.primary-nav .search-form').removeClass('is-active');
		}
	});
	/*----------------------------------------------------------------*\
		TESTIMONIAL SLIDER
	\*----------------------------------------------------------------*/
	$('.testimonial-slider').slick({
		infinite: true,
		slidesToShow: 2,
		slidesToScroll: 1,
		arrows: false,
		dots: true,
		autoplay: true,
  	autoplaySpeed: 5000,
		responsive: [
			{
				breakpoint: 600,
				settings: {
					slidesToShow: 1
				}
			},
		]
	});
	/*----------------------------------------------------------------*\
  	ACTIVATE MENU
	\*----------------------------------------------------------------*/
	$(".post-categories .options").click(function () {
		$(this).toggleClass("is-active");
	});
	/*----------------------------------------------------------------*\
  	ENTRANCE ANIMATION
	\*----------------------------------------------------------------*/
	emergence.init({
		offsetTop: 0,
		offsetBottom: 250,
		offsetLeft: -250,
		offsetRight: -250,
	});
	/*----------------------------------------------------------------*\
  	FRONT PAGE SEGMENT INFO
	\*----------------------------------------------------------------*/
	if($(window).width() <= 1024){
		$(".segments button").click(function () {
			$(this).next(".info").toggleClass("is-active");
		});
	}
	/*----------------------------------------------------------------*\
		PARTNER FILTERING
	\*----------------------------------------------------------------*/
	$(".partner-filter .filter button").click(function () {
		$(".partner-filter .industries").toggleClass("is-active");
	});
	/*----------------------------------------------------------------*\
		SHARE POST
	\*----------------------------------------------------------------*/
	$(window).scroll(function() {    
    var scroll = $(window).scrollTop();    
    if (scroll >= $(window).height()) {
			$("aside.share").addClass('is-active');
    } else {
			$("aside.share").removeClass('is-active');
    }
		if($(window).scrollTop() + $(window).height() > ($(document).height() - 800) ) {
			$("aside.share").removeClass('is-active');
		}
	});
	/*----------------------------------------------------------------*\
		UP NEXT SLIDER
	\*----------------------------------------------------------------*/
	$('.up-next ul').slick({
		infinite: true,
		slidesToShow: 3,
		slidesToScroll: 1,
		arrows: true,
		prevArrow: '<button type="button" class="slick-prev"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M15 2l4 4-6 6 6 6-4 4L5 12z" fill="#242362"/></svg></button>',
		nextArrow: '<button type="button" class="slick-next"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M9 2L5 6l6 6-6 6 4 4 10-10z" fill="#242362"/></svg></button>',
		dots: false,
		responsive: [
			{
				breakpoint: 800,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 1
				}
			},
			{
				breakpoint: 500,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				}
			},
		]
	});
	/*----------------------------------------------------------------*\
  	BLOG NAV TOGGLE
	\*----------------------------------------------------------------*/
	$("button.cat-toggle").click(function () {
		$(this).addClass("is-active");
		$("button.latest-toggle").removeClass("is-active");
		$("section.latest-posts").removeClass("is-active");
		$("section.categories").addClass("is-active");
	});
	$("button.latest-toggle").click(function () {
		$(this).addClass("is-active");
		$("button.cat-toggle").removeClass("is-active");
		$("section.categories").removeClass("is-active");
		$("section.latest-posts").addClass("is-active");
	});
	/*----------------------------------------------------------------*\
		INFINITE SCROLL INIT
	\*----------------------------------------------------------------*/
	$('.infinite-feed').infiniteScroll({
		path: '.next.page-numbers',
		append: '.infinite-feed article',
		button: '.load-more',
		scrollThreshold: false,
		checkLastPage: true,
		history: false,
		status: '.page-load-status',
	});
	/*----------------------------------------------------------------*\
		MODALS
	\*----------------------------------------------------------------*/
	MicroModal.init();
	/*----------------------------------------------------------------*\
		READ PROGRESS
	\*----------------------------------------------------------------*/
	var winHeight = $(window).height();
	var docHeight = $(document).height();
	var extrasHeight = $(".primary-nav-wrap").height() + $("header.post-head").height() + $(".up-next").height() + $(".newsletter").height() + $("#footer").height() + 200 + 120;
	var scrollStart = $(".primary-nav-wrap").height() + $("header.post-head").height() - 120;
	var scrollEnd = scrollStart + $("main").height() + 240;
	var max = docHeight - extrasHeight; 
	$("progress").attr('max', max);
	var value = $(window).scrollTop(); 
	$("progress").attr('value', value);

	$(document).on('scroll', function() {
		value = $(window).scrollTop() - scrollStart;
		$("progress").attr('value', value);
		if ( $(window).scrollTop() < scrollEnd ) {
			$("progress").addClass("is-active");
		} else {
			$("progress").removeClass("is-active");
		}
	});
	/*----------------------------------------------------------------*\
		READING SERIES
	\*----------------------------------------------------------------*/
	$('.reading-series .series').slick({
		infinite: true,
		slidesToShow: 5,
		slidesToScroll: 1,
		arrows: true,
		prevArrow: '<button type="button" class="slick-prev"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M15 2l4 4-6 6 6 6-4 4L5 12z" fill="#949499"/></svg></button>',
		nextArrow: '<button type="button" class="slick-next"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M9 2L5 6l6 6-6 6 4 4 10-10z" fill="#949499"/></svg></button>',
		dots: false,
		// autoplay: true,
  	// autoplaySpeed: 5000,
		responsive: [
			{
				breakpoint: 1050,
				settings: {
					slidesToShow: 4,
					slidesToScroll: 1
				}
			},
			{
				breakpoint: 800,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 1
				}
			},
			{
				breakpoint: 500,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				}
			},
		]
	});
	/*----------------------------------------------------------------*\
		VIDEO SHOWS
	\*----------------------------------------------------------------*/
	$('.shows').slick({
		infinite: true,
		slidesToShow: 5,
		slidesToScroll: 1,
		arrows: true,
		prevArrow: '<button type="button" class="slick-prev"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M15 2l4 4-6 6 6 6-4 4L5 12z" fill="#949499"/></svg></button>',
		nextArrow: '<button type="button" class="slick-next"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M9 2L5 6l6 6-6 6 4 4 10-10z" fill="#949499"/></svg></button>',
		dots: false,
		autoplay: true,
  	autoplaySpeed: 5000,
		responsive: [
			{
				breakpoint: 1050,
				settings: {
					slidesToShow: 4,
					slidesToScroll: 1
				}
			},
			{
				breakpoint: 800,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 1
				}
			},
			{
				breakpoint: 500,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				}
			},
		]
	});
	/*----------------------------------------------------------------*\
		VIDEO TOPIC SLIDERS
	\*----------------------------------------------------------------*/
	$('.topic-slider ul').slick({
		infinite: true,
		slidesToShow: 3,
		slidesToScroll: 1,
		arrows: true,
		prevArrow: '<button type="button" class="slick-prev"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M15 2l4 4-6 6 6 6-4 4L5 12z" fill="#242362"/></svg></button>',
		nextArrow: '<button type="button" class="slick-next"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M9 2L5 6l6 6-6 6 4 4 10-10z" fill="#242362"/></svg></button>',
		dots: false,
		responsive: [
			{
				breakpoint: 800,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 1
				}
			},
			{
				breakpoint: 500,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				}
			},
		]
	});
	/*----------------------------------------------------------------*\
		REMOVE EXTRA TEXT FROM SSO SHORTCODE
	\*----------------------------------------------------------------*/
	$('.primary-nav > a.button + a').text('Login');
	$('.mobile-menu-container > a.join + a').text('Login');
});