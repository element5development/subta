<?php 
/*----------------------------------------------------------------*\

	DEFAULT SINGLE POST TEMPLATE
	This is the page template for the post, for the preview
	look under template-parts.

\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<?php
	//USER ROLE
		if ( is_user_logged_in() ) :
			$user = wp_get_current_user();
			$roles = ( array ) $user->roles;
			$role = $roles[0];
		else :
			$role = 'public';
		endif; 
	//REQUIRED LEVEL
		$accessLevel = get_field('access_level');
	//ACCESS LOGIC
		if ( $accessLevel == 'paid' ) :
			if ( $role == 'public' || $role == 'basic_member_free' ) :
				$acessGranted = false;
			else :
				$acessGranted = true;
			endif;
		elseif ( $accessLevel == 'member' ) : 
			if ( $role == 'public' ) :
				$acessGranted = false;
			else :
				$acessGranted = true;
			endif;
		else :
			$acessGranted = true;
		endif;
?>

<?php if ( $acessGranted == true ) : //HAS ACESS ?>
	<progress id="reading" value="0"></progress>

	<header class="post-head">
		<div class="title is-standard">
			<h1><?php the_title(); ?></h1>
			<?php if ( get_field('post_header_description') ) : ?>
				<p><?php the_field('post_header_description'); ?></p>
			<?php endif; ?>
		</div>
		<?php if ( get_field('display_on_page') && get_field('featured_image') ) : ?>
			<div class="featured-image is-standard">
				<?php $image = get_field('featured_image'); ?>
				<img class="lazyload blur-up" data-expand="250" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['large']; ?>" data-srcset="<?php echo $image['sizes']['small']; ?> 300w, <?php echo $image['sizes']['medium']; ?> 700w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1200w"  alt="<?php echo $image['alt']; ?>">
			</div>
		<?php endif; ?>
		<?php if ( get_field('author') ) : ?>
			<div class="author-partner is-standard 	<?php if ( !get_field('partner_promo') ) : ?>has-no-promo<?php endif; ?>">
				<div class="author">
					<?php if ( get_field('author_headshot') ) : ?>
						<?php $headshot = get_field('author_headshot'); ?>
						<img class="lazyload blur-up" data-expand="250" data-sizes="auto" src="<?php echo $headshot['sizes']['placeholder']; ?>" data-src="<?php echo $headshot['sizes']['large']; ?>" data-srcset="<?php echo $headshot['sizes']['small']; ?> 300w, <?php echo $headshot['sizes']['medium']; ?> 700w, <?php echo $headshot['sizes']['large']; ?> 1000w, <?php echo $headshot['sizes']['xlarge']; ?> 1200w"  alt="<?php echo $headshot['alt']; ?>">
					<?php endif; ?>
					<ul>
						<?php if ( get_field('author') ) : ?>
							<li class="author-name"><?php the_field('author'); ?></li>
						<?php endif; ?>
						<li><?php the_date('M j, Y'); ?></li>
						<?php if ( get_field('estimated_read_time') ) : ?>
							<li><?php the_field('estimated_read_time'); ?> minute read</li>
						<?php endif; ?>
						<?php if ( get_field('partner') ) : ?>
							<?php 
								$partner_posts = get_field('partner');
								foreach( $partner_posts as $partner_post ):
							?>
								<li><a href="<?php echo get_permalink( $partner_post->ID ); ?>"><?php echo get_the_title( $partner_post->ID ); ?></a></li>
							<?php endforeach; ?>
						<?php endif; ?>
					</ul>
				</div>
				<?php if ( get_field('partner_promo') ) : ?>
					<div class="partner-promo">
						<?php the_field('partner_promo'); ?>
					</div>
				<?php endif; ?>
			</div>
		<?php endif; ?>
	</header>

	<main id="main-content">
		<?php if ( have_rows('article') || !empty(get_the_content()) ):  ?>
			<article>
				<?php get_template_part('template-parts/article'); ?>
			</article>
		<?php else : ?>
			<article>
				<section class="is-narrow">
					<h2>Uh Oh. Something is missing.</h2>
					<p>Looks like this page has no content.</p>
				</section>
			</article>
		<?php endif; ?>
	</main>

	<aside class="share">
		<p>Share</p>
		<ul>
			<li>
				<a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>">
					<svg>
						<use xlink:href="#facebook-white" />
					</svg>
				</a>
			</li>
			<li>
				<a target="_blank" href="https://www.linkedin.com/shareArticle?mini=true&amp;url=<?php the_permalink(); ?>&amp;title=&amp;summary=&amp;source=">
					<svg>
						<use xlink:href="#linkedin-white" />
					</svg>
				</a>
			</li>
			<li>
				<a target="_blank" href="https://twitter.com/share?text=&url=<?php the_permalink(); ?>">
					<svg>
						<use xlink:href="#twitter-white" />
					</svg>
				</a>
			</li>
		</ul>
	</aside>

	<aside class="up-next is-wide">
		<h2>Up Next</h2>
		<ul>
			<?php if ( get_field('next_articles') ) : ?>
				<?php 
					$next_posts = get_field('next_articles');
					foreach( $next_posts as $next_post ):
				?>
					<?php 
						if ( get_field('partner') ) : 
							$partner_posts = get_field('partner');
							foreach( $partner_posts as $partner_post ):
								$partner_name = get_the_title( $partner_post->ID );
							endforeach;
						endif;
					?>
					<li>
						<a href="<?php echo get_permalink( $next_post->ID ); ?>">
							<?php if ( get_field('featured_image', $next_post->ID) ) : ?>
								<?php $image = get_field('featured_image', $next_post->ID); ?>
								<img class="lazyload" data-expand="250" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['large']; ?>" data-srcset="<?php echo $image['sizes']['small']; ?> 300w, <?php echo $image['sizes']['medium']; ?> 700w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1200w"  alt="<?php echo $image['alt']; ?>">
							<?php else : ?>
								<img class="lazyload" data-expand="250" data-sizes="auto" src="/wp-content/themes/subta/dist/images/default-post-graphic.png"  alt="SUBTA -">
							<?php endif; ?>
							<p>
								<?php echo get_the_date('F d', $next_post->ID); ?>
								<?php if ( get_field('estimated_read_time', $next_post->ID) ) : ?>
									| <?php the_field('estimated_read_time', $next_post->ID); ?> minute read
								<?php endif; ?>
							</p>
							<h3><?php echo get_the_title( $next_post->ID ); ?></h3>
							<?php 
								if (get_field('partner', $next_post->ID)) :
									$partner_posts = get_field('partner', $next_post->ID);
									foreach( $partner_posts as $partner_post ):
										$partner_name = get_the_title( $partner_post->ID );
									endforeach;
								endif;
							?>
							<?php if ( get_field('author', $next_post->ID) || get_field('partner', $next_post->ID) ) : ?>
								<p>Written by <?php the_field('author', $next_post->ID); ?><?php if (get_field('partner', $next_post->ID)) : ?> from <?php echo $partner_name; ?><?php endif; ?></p>
							<?php endif; ?>
						</a>
					</li>
				<?php endforeach; ?>	
			<?php elseif ( has_term('', 'postseries') ) : ?>
				<?php 
					$terms = get_the_terms( $post->ID , 'postseries' );
					$args = array(
						'posts_per_page' => -1, 
						'post__not_in' => array(get_the_ID()),
						'tax_query' => array(
							array (
									'taxonomy' => 'postseries',
									'field' => 'term_id',
									'terms' => $terms[0]->term_id,
							)
						),
					);
					$wp_query = new WP_Query( $args );
				?>
				<?php while($wp_query->have_posts()) : $wp_query->the_post(); ?>
					<li>
						<a href="<?php echo get_permalink(); ?>">
							<?php if ( get_field('featured_image') ) : ?>
								<?php $image = get_field('featured_image'); ?>
								<img class="lazyload" data-expand="250" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['large']; ?>" data-srcset="<?php echo $image['sizes']['small']; ?> 300w, <?php echo $image['sizes']['medium']; ?> 700w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1200w"  alt="<?php echo $image['alt']; ?>">
							<?php else : ?>
								<img class="lazyload" data-expand="250" data-sizes="auto" src="/wp-content/themes/subta/dist/images/default-post-graphic.png"  alt="SUBTA -">
							<?php endif; ?>
							<p>
								<?php echo get_the_date('F d'); ?>
								<?php if ( get_field('estimated_read_time') ) : ?>
									| <?php the_field('estimated_read_time'); ?> minute read
								<?php endif; ?>
							</p>
							<h3><?php echo get_the_title(); ?></h3>
							<?php 
								if (get_field('partner')) :
									$partner_posts = get_field('partner');
									foreach( $partner_posts as $partner_post ):
										$partner_name = get_the_title( $partner_post->ID );
									endforeach;
								endif;
							?>
							<?php if ( get_field('author') || get_field('partner') ) : ?>
								<p>Written by <?php the_field('author'); ?><?php if (get_field('partner')) : ?> from <?php echo $partner_name; ?><?php endif; ?></p>
							<?php endif; ?>
						</a>
					</li>
				<?php endwhile; wp_reset_postdata(); ?>
			<?php else : ?>
				<?php 
					$categories = get_the_category();	
					$cat_id = $categories[0]->term_id;
					$args = array(
						'post__not_in' => array(get_the_ID()),
						'posts_per_page' => 6, 
						'cat' => $cat_id,
					);
					$wp_query = new WP_Query( $args );
					while($wp_query->have_posts()) : $wp_query->the_post();
				?>
					<li>
						<a href="<?php echo get_permalink(); ?>">
							<div class="img-wrap">
								<?php if ( get_field('featured_image') ) : ?>
									<?php $image = get_field('featured_image'); ?>
									<img class="lazyload" data-expand="250" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['large']; ?>" data-srcset="<?php echo $image['sizes']['small']; ?> 300w, <?php echo $image['sizes']['medium']; ?> 700w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1200w"  alt="<?php echo $image['alt']; ?>">
								<?php else : ?>
									<img class="lazyload" data-expand="250" data-sizes="auto" src="/wp-content/themes/subta/dist/images/default-post-graphic.png"  alt="SUBTA -">
								<?php endif; ?>
							</div>
							<p>
								<?php echo get_the_date('F d',); ?>
								<?php if ( get_field('estimated_read_time') ) : ?>
									| <?php the_field('estimated_read_time'); ?> minute read
								<?php endif; ?>
							</p>
							<h3><?php echo get_the_title(); ?></h3>
							<?php 
								if (get_field('partner')) :
									$partner_posts = get_field('partner');
									foreach( $partner_posts as $partner_post ):
										$partner_name = get_the_title( $partner_post->ID );
									endforeach;
								endif;
							?>
							<?php if ( get_field('author') || get_field('partner') ) : ?>
								<p>Written by <?php the_field('author'); ?><?php if (get_field('partner')) : ?> from <?php echo $partner_name; ?><?php endif; ?></p>
							<?php endif; ?>
						</a>
					</li>
				<?php endwhile; wp_reset_postdata(); ?>
			<?php endif; ?>
		</ul>
	</aside>

	<aside class="newsletter">
		<h2>More News Direct to Your Inbox</h2>
		<p>Be notified via email with the latest articles, industry news, partner resources and much more.</p>
		<?php echo do_shortcode('[gravityform id="3" title="false" description="false"]'); ?>
	</aside>

	<?php get_template_part('template-parts/sections/post-footer'); ?>

<?php else: ?>
	<div class="gated">
		<header>
			<div class="title is-narrow">
				<p><?php the_date('F j, Y'); ?></p>
				<h1><?php the_title(); ?></h1>
			</div>
		</header>
		<main id="main-content">
			<article>
				<?php if ( get_field('preview') ) : ?>
					<section class="is-narrow">
						<?php the_field('preview'); ?>
					</section>
				<?php endif ; ?>
				<?php if ( get_field('featured_image') ) : ?>
					<section class="is-standard">
						<?php $image = get_field('featured_image'); ?>
						<img class="lazyload blur-up" data-expand="250" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['large']; ?>" data-srcset="<?php echo $image['sizes']['small']; ?> 300w, <?php echo $image['sizes']['medium']; ?> 700w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1200w"  alt="<?php echo $image['alt']; ?>">
					</section>
				<?php endif; ?>
			</article>
		</main>
	</div>
	<aside class="gated">
		<h2 class="is-standard">Want to keep reading?</h2>
		<div class="is-standard">
			<?php $i = 0; while( have_rows('gated_cards', 'options') ) : the_row(); $i++; ?>
				<div>
					<h3><?php the_sub_field('title'); ?></h3>
					<p class="price">
						<?php if ( get_sub_field('full_price') ) : ?>
							<s><?php the_sub_field('full_price'); ?></s>
						<?php endif; ?>
						<?php the_sub_field('price'); ?>
					</p>
					<p><?php the_sub_field('description'); ?></p>
					<?php if ( get_sub_field('button') ) : ?>
						<?php 
							$link = get_sub_field('button');
							$link_url = $link['url'];
							$link_title = $link['title'];
							$link_target = $link['target'] ? $link['target'] : '_self';
						?>
						<a class="button <?php if ( $i == 1 ) : ?>is-green<?php endif; ?>" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a>
					<?php endif; ?>
				</div>
			<?php endwhile; ?>
		</div>
	</aside>
<?php endif; ?>

<?php get_footer(); ?>