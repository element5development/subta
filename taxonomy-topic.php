<?php 
/*----------------------------------------------------------------*\

	DEFAULT CATEGORY ARCHIVE TEMPLATE

\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<?php 
	$terms = get_terms(
		array(
				'taxonomy'   => 'topic',
				'hide_empty' => true,
		)
	);
?>
<!-- PAGE TITLE AND BANNER -->
<header class="post-head is-extra-wide">
	<h1><span>Videos on</span> <?php single_cat_title() ?></h1>
	<?php echo category_description(); ?>
	<nav class="post-categories">
		Watch <span>videos on</span>
		<div class="option-wrap">
			<div class="options">
				<div class="current-cat"><?php single_cat_title() ?></div>
				<a href="/video">View All</a>
				<?php foreach ( $terms as $term ) :	?>
					<a href="<?php echo get_category_link( $term->term_id ); ?>"><?php echo $term->name; ?></a>
				<?php endforeach; ?>
			</div>
		</div>
	</nav>
</header>

<main id="main-content" class="full-width">
	<!-- PARTNER GRID -->
	<?php if (have_posts()) : ?>
		<section class="video-feed is-extra-wide">
			<?php	while ( have_posts() ) : the_post(); ?>
				<article class="video-post-preview">
					<a href="<?php echo get_permalink(); ?>">
						<?php if ( get_field('video_thumbnail') ) : ?>
							<?php $image = get_field('video_thumbnail'); ?>
							<img class="lazyload" data-expand="250" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['large']; ?>" data-srcset="<?php echo $image['sizes']['small']; ?> 300w, <?php echo $image['sizes']['medium']; ?> 700w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1200w"  alt="<?php echo $image['alt']; ?>">
						<?php else : ?>
							<img class="lazyload" data-expand="250" data-sizes="auto" src="/wp-content/themes/subta/dist/images/default-post-graphic.png"  alt="SUBTA -">
						<?php endif; ?>
						<?php if ( get_field('access_level') != 'public' ) : ?>
							<div class="gated">
								<svg>
									<use xlink:href="#gated" />
								</svg>
								<span>Member Only</span>
							</div>
						<?php endif; ?>
						<h3><?php echo get_the_title(); ?></h3>
						<p class="meta">
							<?php if ( get_field('run_time') ) : ?>
								<span><?php the_field('run_time'); ?> min</span>
							<?php endif; ?>
							<?php echo get_the_date('F j, Y'); ?>
						</p>
					</a>
				</article>
			<?php endwhile; ?>
		</section>
	<?php endif; ?>
	<?php clean_pagination(); ?>
</main>

<aside class="sub-or-join is-extra-wide">
	<div class="newsletter">
		<h2>Direct to Your Inbox</h2>
		<p>Be notified via email with the latest articles, industry news, partner resources and much more.</p>
		<?php echo do_shortcode('[gravityform id="3" title="false" description="false"]'); ?>
	</div>
	<div class="join">
		<h2>Unlock Everything</h2>
		<p>SUBTA is an ever-growing community of innovators, entrepreneurs, thought leaders and dedicated teams that are eager to scale their businesses and catalyze the subscription industry. Sound like you?</p>	
		<a href="/join" class="button is-yellow">Join SUBTA</a>
	</div>
</aside>

<?php get_template_part('template-parts/sections/post-footer'); ?>

<?php get_footer(); ?>