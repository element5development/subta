<?php 
/*----------------------------------------------------------------*\

	DEFAULT SINGLE POST TEMPLATE
	This is the page template for the post, for the preview
	look under template-parts.

\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php //IF ACCESSING PAGE WITHOUT LOGIN FORM
	if ( has_term( 'subsummit-2021', 'show' ) && !isset($_COOKIE['subsummit_2021_access']) ) :
		header('Location: https://subta.com/access-subsummit-2021/');
		exit;
	endif; 
?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<?php
	//USER ROLE
		if ( is_user_logged_in() ) :
			$user = wp_get_current_user();
			$roles = ( array ) $user->roles;
			$role = $roles[0];
		else :
			$role = 'public';
		endif; 
	//REQUIRED LEVEL
		$accessLevel = get_field('access_level');
	//ACCESS LOGIC
		if ( $accessLevel == 'paid' ) :
			if ( $role == 'public' || $role == 'basic_member_free' ) :
				$acessGranted = false;
			else :
				$acessGranted = true;
			endif;
		elseif ( $accessLevel == 'member' ) : 
			if ( $role == 'public' ) :
				$acessGranted = false;
			else :
				$acessGranted = true;
			endif;
		else :
			$acessGranted = true;
		endif;
?>

<?php if ( $acessGranted == true ) : //HAS ACESS ?>
	<header class="post-head">
		<div class="video-embed is-extra-wide">
			<?php the_field('video_embed'); ?>
		</div>
		<div class="title is-narrow">
			<h1><?php the_title(); ?></h1>
			<p class="meta">
				<?php if ( get_field('run_time') ) : ?>
					<span><?php the_field('run_time'); ?> min</span>
				<?php endif; ?>
				<?php the_date('F j, Y'); ?>
			</p>
			<?php if ( get_field('post_header_description') ) : ?>
				<p><?php the_field('post_header_description'); ?></p>
			<?php endif; ?>
		</div>
	</header>

	<main id="main-content">
		<article>
			<?php if ( get_field('guest') ) : ?>
				<section class="guests is-extra-wide">
					<?php while( have_rows('guest') ) : the_row(); ?>
						<div class="guest">
							<?php $image = get_sub_field('headshot'); ?>
							<img class="lazyload" data-expand="250" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['large']; ?>" data-srcset="<?php echo $image['sizes']['small']; ?> 300w, <?php echo $image['sizes']['medium']; ?> 700w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1200w"  alt="<?php echo $image['alt']; ?>">
							<div class="details">
								<?php $image = get_sub_field('company_logo'); ?>
								<img class="lazyload" data-expand="250" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['large']; ?>" data-srcset="<?php echo $image['sizes']['small']; ?> 300w, <?php echo $image['sizes']['medium']; ?> 700w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1200w"  alt="<?php echo $image['alt']; ?>">
								<h3>
									<?php the_sub_field('name'); ?>
									<?php if ( get_sub_field('job_title') ) : ?>
										<span><?php the_sub_field('job_title'); ?></span>
									<?php endif; ?>
								</h3>
							</div>
						</div>
					<?php endwhile; ?>
				</section>
			<?php endif; ?>
			<?php if ( have_rows('article') || !empty(get_the_content()) ):  ?>
				<?php get_template_part('template-parts/article'); ?>
			<?php endif; ?>
		</article>
	</main>

	<aside class="share">
		<p>Share</p>
		<ul>
			<li>
				<a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>">
					<svg>
						<use xlink:href="#facebook-white" />
					</svg>
				</a>
			</li>
			<li>
				<a target="_blank" href="https://www.linkedin.com/shareArticle?mini=true&amp;url=<?php the_permalink(); ?>&amp;title=&amp;summary=&amp;source=">
					<svg>
						<use xlink:href="#linkedin-white" />
					</svg>
				</a>
			</li>
			<li>
				<a target="_blank" href="https://twitter.com/share?text=&url=<?php the_permalink(); ?>">
					<svg>
						<use xlink:href="#twitter-white" />
					</svg>
				</a>
			</li>
		</ul>
	</aside>

	<aside class="up-next is-wide">
		<h2>Up Next</h2>
		<ul>
			<?php if ( get_field('up_next') ) : ?>
				<?php 
					$next_posts = get_field('up_next');
					foreach( $next_posts as $next_post ):
				?>
					<li class="video-post-preview">
						<a href="<?php echo get_permalink( $next_post->ID ); ?>">
							<?php if ( get_field('video_thumbnail', $next_post->ID) ) : ?>
								<?php $image = get_field('video_thumbnail', $next_post->ID); ?>
								<img class="lazyload" data-expand="250" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['large']; ?>" data-srcset="<?php echo $image['sizes']['small']; ?> 300w, <?php echo $image['sizes']['medium']; ?> 700w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1200w"  alt="<?php echo $image['alt']; ?>">
							<?php else : ?>
								<img class="lazyload" data-expand="250" data-sizes="auto" src="/wp-content/themes/subta/dist/images/default-post-graphic.png"  alt="SUBTA -">
							<?php endif; ?>
							<?php if ( get_field('access_level') != 'public' ) : ?>
								<div class="gated">
									<svg>
										<use xlink:href="#gated" />
									</svg>
									<span>Member Only</span>
								</div>
							<?php endif; ?>
							<h3><?php echo get_the_title( $next_post->ID ); ?></h3>
							<p class="meta">
								<?php if ( get_field('run_time', $next_post->ID) ) : ?>
									<span><?php the_field('run_time', $next_post->ID); ?> min</span>
								<?php endif; ?>
								<?php echo get_the_date('F j, Y'); ?>
							</p>
						</a>
					</li>
				<?php endforeach; ?>	
			<?php elseif ( has_term('', 'show') ) : ?>
				<?php 
					$terms = get_the_terms( $post->ID , 'show' );
					$args = array(
						'post_type' => 'video',
						'posts_per_page' => 8, 
						'post__not_in' => array(get_the_ID()),
						'tax_query' => array(
							array (
									'taxonomy' => 'show',
									'field' => 'term_id',
									'terms' => $terms[0]->term_id,
							)
						),
					);
					$wp_query = new WP_Query( $args );
				?>
				<?php while($wp_query->have_posts()) : $wp_query->the_post(); ?>
					<li class="video-post-preview">	
						<a href="<?php echo get_permalink( ); ?>">
							<?php if ( get_field('video_thumbnail') ) : ?>
								<?php $image = get_field('video_thumbnail'); ?>
								<img class="lazyload" data-expand="250" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['large']; ?>" data-srcset="<?php echo $image['sizes']['small']; ?> 300w, <?php echo $image['sizes']['medium']; ?> 700w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1200w"  alt="<?php echo $image['alt']; ?>">
							<?php else : ?>
								<img class="lazyload" data-expand="250" data-sizes="auto" src="/wp-content/themes/subta/dist/images/default-post-graphic.png"  alt="SUBTA -">
							<?php endif; ?>
							<h3><?php echo get_the_title( ); ?></h3>
							<p class="meta">
								<?php if ( get_field('run_time') ) : ?>
									<span><?php the_field('run_time'); ?> min</span>
								<?php endif; ?>
								<?php echo get_the_date('F j, Y'); ?>
							</p>
						</a>
					</li>
				<?php endwhile; wp_reset_postdata(); ?>
			<?php elseif ( has_term('', 'topic') ) : ?>
				<?php 
					$terms = get_the_terms( $post->ID , 'topic' );
					$args = array(
						'post_type' => 'video',
						'posts_per_page' => 8, 
						'post__not_in' => array(get_the_ID()),
						'tax_query' => array(
							array (
									'taxonomy' => 'topic',
									'field' => 'term_id',
									'terms' => $terms[0]->term_id,
							)
						),
					);
					$wp_query = new WP_Query( $args );
				?>
				<?php while($wp_query->have_posts()) : $wp_query->the_post(); ?>
					<li class="video-post-preview">	
						<a href="<?php echo get_permalink( ); ?>">
							<?php if ( get_field('video_thumbnail') ) : ?>
								<?php $image = get_field('video_thumbnail'); ?>
								<img class="lazyload" data-expand="250" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['large']; ?>" data-srcset="<?php echo $image['sizes']['small']; ?> 300w, <?php echo $image['sizes']['medium']; ?> 700w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1200w"  alt="<?php echo $image['alt']; ?>">
							<?php else : ?>
								<img class="lazyload" data-expand="250" data-sizes="auto" src="/wp-content/themes/subta/dist/images/default-post-graphic.png"  alt="SUBTA -">
							<?php endif; ?>
							<h3><?php echo get_the_title( ); ?></h3>
							<p class="meta">
								<?php if ( get_field('run_time') ) : ?>
									<span><?php the_field('run_time'); ?> min</span>
								<?php endif; ?>
								<?php echo get_the_date('F j, Y'); ?>
							</p>
						</a>
					</li>
				<?php endwhile; wp_reset_postdata(); ?>
			<?php else : ?>
				<?php 
					$args = array(
						'post_type' => 'video',
						'posts_per_page' => 8, 
						'post__not_in' => array(get_the_ID()),
					);
					$wp_query = new WP_Query( $args );
				?>
				<?php while($wp_query->have_posts()) : $wp_query->the_post(); ?>
					<li class="video-post-preview">	
						<a href="<?php echo get_permalink( ); ?>">
							<?php if ( get_field('video_thumbnail') ) : ?>
								<?php $image = get_field('video_thumbnail'); ?>
								<img class="lazyload" data-expand="250" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['large']; ?>" data-srcset="<?php echo $image['sizes']['small']; ?> 300w, <?php echo $image['sizes']['medium']; ?> 700w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1200w"  alt="<?php echo $image['alt']; ?>">
							<?php else : ?>
								<img class="lazyload" data-expand="250" data-sizes="auto" src="/wp-content/themes/subta/dist/images/default-post-graphic.png"  alt="SUBTA -">
							<?php endif; ?>
							<h3><?php echo get_the_title( ); ?></h3>
							<p class="meta">
								<?php if ( get_field('run_time') ) : ?>
									<span><?php the_field('run_time'); ?> min</span>
								<?php endif; ?>
								<?php echo get_the_date('F j, Y'); ?>
							</p>
						</a>
					</li>
				<?php endwhile; wp_reset_postdata(); ?>
			<?php endif; ?>
		</ul>
	</aside>

	<aside class="newsletter">
		<h2>More News Direct to Your Inbox</h2>
		<p>Be notified via email with the latest articles, industry news, partner resources and much more.</p>
		<?php echo do_shortcode('[gravityform id="3" title="false" description="false"]'); ?>
	</aside>

	<?php get_template_part('template-parts/sections/post-footer'); ?>

<?php else: ?>
	<div class="gated">
		<header class="post-head">
			<div class="video-embed is-extra-wide">
				<?php the_field('trailer_embed'); ?>
			</div>
			<div class="title is-narrow">
				<h1><?php the_title(); ?></h1>
				<p class="meta">
					<?php if ( get_field('run_time') ) : ?>
						<span><?php the_field('run_time'); ?> min</span>
					<?php endif; ?>
					<?php the_date('F j, Y'); ?>
				</p>
				<?php if ( get_field('post_header_description') ) : ?>
					<p><?php the_field('post_header_description'); ?></p>
				<?php endif; ?>
			</div>
		</header>
		<main>
			<article>
				<?php if ( get_field('guest') ) : ?>
					<section class="guests is-extra-wide">
						<?php while( have_rows('guest') ) : the_row(); ?>
							<div class="guest">
								<?php $image = get_sub_field('headshot'); ?>
								<img class="lazyload" data-expand="250" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['large']; ?>" data-srcset="<?php echo $image['sizes']['small']; ?> 300w, <?php echo $image['sizes']['medium']; ?> 700w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1200w"  alt="<?php echo $image['alt']; ?>">
								<div class="details">
									<?php $image = get_sub_field('company_logo'); ?>
									<img class="lazyload" data-expand="250" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['large']; ?>" data-srcset="<?php echo $image['sizes']['small']; ?> 300w, <?php echo $image['sizes']['medium']; ?> 700w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1200w"  alt="<?php echo $image['alt']; ?>">
									<h3>
										<?php the_sub_field('name'); ?>
										<?php if ( get_sub_field('job_title') ) : ?>
											<span><?php the_sub_field('job_title'); ?></span>
										<?php endif; ?>
									</h3>
								</div>
							</div>
						<?php endwhile; ?>
					</section>
				<?php endif; ?>
			</article>
		</main>
	</div>
	<aside class="gated">
		<h2 class="is-standard">Want to keep watching?</h2>
		<div class="is-standard">
			<?php $i = 0; while( have_rows('gated_cards', 'options') ) : the_row(); $i++; ?>
				<div>
					<h3><?php the_sub_field('title'); ?></h3>
					<p class="price">
						<?php if ( get_sub_field('full_price') ) : ?>
							<s><?php the_sub_field('full_price'); ?></s>
						<?php endif; ?>
						<?php the_sub_field('price'); ?>
					</p>
					<p><?php the_sub_field('description'); ?></p>
					<?php if ( get_sub_field('button') ) : ?>
						<?php 
							$link = get_sub_field('button');
							$link_url = $link['url'];
							$link_title = $link['title'];
							$link_target = $link['target'] ? $link['target'] : '_self';
						?>
						<a class="button <?php if ( $i == 1 ) : ?>is-green<?php endif; ?>" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a>
					<?php endif; ?>
				</div>
			<?php endwhile; ?>
		</div>
	</aside>
<?php endif; ?>

<?php get_footer(); ?>