<?php /*
THE HEADER TEMPLATE FOR OUR THEME
*/ ?>

<!doctype html>
<html xml:lang="en" lang="en">

<head>
	<!-------------------------->
	<!-- PLACE ANALYTICS CODE -->
	<!-------------------------->
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="google-site-verification" content="hHlXqfPZEuOrKFEzm-02ogvGvynu0lXehp8OXxmoA80" />
	<meta name="facebook-domain-verification" content="a7gxz2itbsqr6qg5vqzyb7duhi1brt" />
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<title>
		<?php wp_title(''); ?>
	</title>
	<?php wp_head(); ?>
	<!-- Google Tag Manager -->
		<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
		new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
		j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
		'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
		})(window,document,'script','dataLayer','GTM-T654F3Q');</script>
	<!-- End Google Tag Manager -->
	<!-- Google Optimize -->
	<script src="https://www.googleoptimize.com/optimize.js?id=OPT-PWQHZVW"></script>
	<!-- End of Google Optimize -->
	<!-- Facebook Pixel Code -->
	<script>
	!function(f,b,e,v,n,t,s)
	{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
	n.callMethod.apply(n,arguments):n.queue.push(arguments)};
	if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
	n.queue=[];t=b.createElement(e);t.async=!0;
	t.src=v;s=b.getElementsByTagName(e)[0];
	s.parentNode.insertBefore(t,s)}(window, document,'script',
	'https://connect.facebook.net/en_US/fbevents.js');
	fbq('init', '721883984903321');
		<?php if ( is_page(62811) ) : ?>
			fbq('init', '620822334744153');
		<?php endif; ?>
	fbq('track', 'PageView');
	</script>
	<noscript>
		<img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=721883984903321&ev=PageView&noscript=1" />
		<img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=620822334744153&ev=PageView&noscript=1" />
	</noscript>
	<!-- End Facebook Pixel Code -->
</head>

<body <?php body_class(); ?>>

	<?php if( current_user_can('editor') || current_user_can('administrator') ) :  ?>
		<!-- if user is admin or editor dont place any annlytic tracking on site. -->
	<?php else : ?>
		<!-- Google Tag Manager (noscript) -->
		<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-T654F3Q"
		height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
		<!-- End Google Tag Manager (noscript) -->
	<?php endif; ?>

	<a id="skip-to-content" href="#main-content">Skip to main content</a>

	<!-- THE ICONS THAT ARE CALLED BY SVGS -->
	<?php get_template_part('template-parts/icon-set'); ?>