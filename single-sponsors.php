<?php 
/*----------------------------------------------------------------*\

	DEFAULT SINGLE POST TEMPLATE
	This is the page template for the post, for the preview
	look under template-parts.

\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>


<header class="post-head">
	<div class="is-standard">
		<?php if ( get_field('logo') ) : ?>
			<?php $image = get_field('logo'); ?>
			<img class="logo lazyload blur-up" data-expand="250" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['large']; ?>" data-srcset="<?php echo $image['sizes']['small']; ?> 300w, <?php echo $image['sizes']['medium']; ?> 700w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1200w"  alt="<?php echo $image['alt']; ?>">
		<?php endif; ?>
		<?php if ( get_field('description') ) : ?>
			<h1><?php the_field('description'); ?></h1>
		<?php endif; ?>
		<?php if ( wp_get_post_terms( get_the_ID() , 'specialization') ) : ?>
			<dl>
				<dt>specializes in</dt>
				<?php 	
					$terms = wp_get_post_terms( get_the_ID() , 'specialization');
					foreach ( $terms as $term ) {
						echo '<dd>'.$term->name.'</dd>';
					}
				?>
			</dl>
		<?php endif; ?>
	</div>
</header>

<main id="main-content">
	<?php if ( get_field('gravity_form_id') ) : ?>
		<aside>
			<?php if ( get_field('discount_title') ) : ?>
				<h3><?php the_field('discount_title'); ?></h3>
			<?php endif; ?>
			<?php if ('additional_details') : ?>
				<p><?php the_field('additional_details'); ?></p>
			<?php endif; ?>
			<?php if ( get_field('representative_headshot') && get_field('representative_name') ) : ?>
				<div class="partner-rep">
					<?php $image = get_field('representative_headshot'); ?>
					<figure>
						<img class="lazyload blur-up" data-expand="250" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['large']; ?>" data-srcset="<?php echo $image['sizes']['small']; ?> 350w, <?php echo $image['sizes']['medium']; ?> 700w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1200w"  alt="<?php echo $image['alt']; ?>">
					</figure>
					<p><span><?php the_title(); ?> Rep </span><?php the_field('representative_name'); ?></p>
				</div>
			<?php endif; ?>
			<?php echo do_shortcode('[gravityform id="'.get_field('gravity_form_id').'" title="false" description="false"]'); ?>
		</aside>
	<?php endif; ?>
	<?php if ( have_rows('article') || !empty(get_the_content()) ):  ?>
		<article>
			<?php get_template_part('template-parts/article'); ?>
			<?php if ( have_rows('social_profiles') ) : ?>
				<div class="social">
					<p><b>Want to learn more?</b></p>
					<?php while( have_rows('social_profiles') ) : the_row(); ?>
							<?php 
								$link = get_sub_field('profile'); 
								$link_url = $link['url'];
								$link_title = $link['title'];
								$link_target = $link['target'] ? $link['target'] : '_self';
							?>
							<a href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a>
					<?php endwhile; ?>
				</div>
			<?php endif; ?>
		</article>
	<?php else : ?>
		<article>
			<section class="is-narrow">
				<h2>Uh Oh. Something is missing.</h2>
				<p>Looks like this page has no content.</p>
			</section>
		</article>
	<?php endif; ?>
	<?php if ( get_field('guest_author_posts') ) : ?>
		<section class="partner-posts post-grid is-standard">
			<h2>Articles written by <?php the_title(); ?></h2>
			<?php $featured_posts = get_field('guest_author_posts'); ?>
			<?php foreach( $featured_posts as $featured_post ): ?>
				<a class="post-preview" href="<?php echo get_permalink( $featured_post->ID ); ?>">
					<article class="post-preview">
						<?php 
							$thumb_id = get_post_thumbnail_id($featured_post->ID);
							$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'thumbnail', true);
							$thumb_url = $thumb_url_array[0];
						?>
						<?php if ( get_post_thumbnail_id($featured_post->ID) ) : ?>
							<img src="<?php echo $thumb_url; ?>"  />
						<?php else : ?>
							<div class="image"></div>
						<?php endif; ?>
						<div>
							<h3><?php echo get_the_title($featured_post->ID); ?></h3>
							<p><?php echo get_post_meta($featured_post->ID, '_yoast_wpseo_metadesc', true); ?></p>
							<div class="button is-arrow">Read the Rest</div>
						</div>
					</article>
				</a>
			<?php endforeach; ?>
		</section>
	<?php endif; ?>
</main>

<?php get_template_part('template-parts/sections/post-footer'); ?>

<?php get_footer(); ?>