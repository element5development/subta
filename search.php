<?php 
/*----------------------------------------------------------------*\

	SEARCH RESULTS

\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/cookie-bar'); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<header class="is-standard">
	<h1>Search</h1>
	<?php if ( get_search_query() ) : ?>
		<?php global $wp_query; ?>
		<p><?php echo $wp_query->found_posts; ?> results for “<?php echo get_search_query(); ?>”</p>
	<?php endif; ?>
	<form role="search" method="get" action="<?php echo get_site_url(); ?>">
		<input type="search" placeholder="<?php echo esc_attr_x( 'Search SUBTA…', 'placeholder' ) ?>" name="s" title="<?php echo esc_attr_x( 'Search for:', 'label' ) ?>" />
		<button type="submit">
			<svg>
				<use xlink:href="#search" />
			</svg>
		</button>
	</form>
</header>

<main id="main-content">
	<!-- PARTNER GRID -->
	<?php if ( have_posts() && get_search_query() ) : ?>
		<section class="post-grid is-standard">
			<?php	while ( have_posts() ) : the_post(); ?>
				<a class="post-preview" href="<?php echo get_permalink($post->ID); ?>">
					<article class="post-preview">
						<?php 
							$thumb_id = get_post_thumbnail_id($post->ID);
							$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'thumbnail', true);
							$thumb_url = $thumb_url_array[0];
						?>
						<?php if ( get_post_thumbnail_id($post->ID) ) : ?>
							<img src="<?php echo $thumb_url; ?>"  />
						<?php else : ?>
							<div class="image"></div>
						<?php endif; ?>
						<div>
							<h3><?php echo get_the_title($post->ID); ?></h3>
							<p><?php echo get_post_meta(get_the_ID(), '_yoast_wpseo_metadesc', true); ?></p>
							<div class="button is-arrow">Read the Rest</div>
						</div>
					</article>
				</a>
			<?php endwhile; ?>
		</section>
		<?php clean_pagination(); ?>
	<?php endif; ?>
	<?php if ( !have_posts() && get_search_query() ) : ?>
		<section class="no-results is-standard">
			<h3>Less is more, but we need more information. Try searching a different phrase.</h3>
		</section>
	<?php endif; ?>
	<?php if ( !get_search_query() || !have_posts() ) : ?>
		<section class="recommended post-grid is-standard">
			<h5>Recommended Content</h5>
			<?php $featured_posts = get_field('recommended', 'options'); ?>
			<?php foreach( $featured_posts as $featured_post ): ?>
				<a class="post-preview" href="<?php echo get_permalink($featured_post->ID); ?>">
					<article class="post-preview">
						<?php 
							$thumb_id = get_post_thumbnail_id($featured_post->ID);
							$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'thumbnail', true);
							$thumb_url = $thumb_url_array[0];
						?>
						<?php if ( get_post_thumbnail_id($featured_post->ID) ) : ?>
							<img src="<?php echo $thumb_url; ?>"  />
						<?php else : ?>
							<div class="image"></div>
						<?php endif; ?>
						<div>
							<h3><?php echo get_the_title($featured_post->ID); ?></h3>
							<p><?php echo get_post_meta($featured_post->ID, '_yoast_wpseo_metadesc', true); ?></p>
							<div class="button is-arrow">Read the Rest</div>
						</div>
					</article>
				</a>
			<?php endforeach; ?>
		</section>
	<?php endif; ?>
</main>

<?php get_template_part('template-parts/sections/post-footer'); ?>

<?php get_footer(); ?>